! function(t) {
    if ("object" == typeof exports && "undefined" != typeof module) module.exports = t();
    else if ("function" == typeof define && define.amd) define([], t);
    else {
        var e;
        e = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, e.BinauralFIR = t()
    }
}(function() {
    return function t(e, n, i) {
        function o(s, a) {
            if (!n[s]) {
                if (!e[s]) {
                    var u = "function" == typeof require && require;
                    if (!a && u) return u(s, !0);
                    if (r) return r(s, !0);
                    var l = new Error("Cannot find module '" + s + "'");
                    throw l.code = "MODULE_NOT_FOUND", l
                }
                var c = n[s] = {
                    exports: {}
                };
                e[s][0].call(c.exports, function(t) {
                    var n = e[s][1][t];
                    return o(n ? n : t)
                }, c, c.exports, t, e, n, i)
            }
            return n[s].exports
        }
        for (var r = "function" == typeof require && require, s = 0; s < i.length; s++) o(i[s]);
        return o
    }({
        1: [function(t, e, n) {
            e.exports = t("./dist/binaural-fir")
        }, {
            "./dist/binaural-fir": 2
        }],
        2: [function(t, e, n) {
            "use strict";
            var i = t("babel-runtime/helpers/create-class")["default"],
                o = t("babel-runtime/helpers/class-call-check")["default"],
                r = t("babel-runtime/helpers/interop-require-default")["default"];
            Object.defineProperty(n, "__esModule", {
                value: !0
            });
            var s = t("kdt"),
                a = r(s),
                u = function() {
                    function t(e) {
                        o(this, t), this.audioContext = e.audioContext, this.hrtfDataset = [], this.hrtfDatasetLength = 0, this.tree = void 0, this.position = {}, this.crossfadeDuration = .02, this.input = this.audioContext.createGain(), this.state = "A", this.target = void 0, this.pendingPosition = void 0, this.convolverA = new l({
                            audioContext: this.audioContext
                        }), this.convolverA.gain.setTargetAtTime(1, this.audioContext.currentTime, 0.5), this.input.connect(this.convolverA.input), this.convolverB = new l({
                            audioContext: this.audioContext
                        }), this.convolverB.gain.setTargetAtTime(0, this.audioContext.currentTime, 0.5), this.input.connect(this.convolverB.input)
                    }
                    return i(t, [{
                        key: "connect",
                        value: function(t) {
                            return this.convolverA.connect(t), this.convolverB.connect(t), this
                        }
                    }, {
                        key: "disconnect",
                        value: function(t) {
                            return this.convolverA.disconnect(t), this.convolverB.disconnect(t), this
                        }
                    }, {
                        key: "distance",
                        value: function(t, e) {
                            return Math.pow(t.x - e.x, 2) + Math.pow(t.y - e.y, 2) + Math.pow(t.z - e.z, 2)
                        }
                    }, {
                        key: "setPosition",
                        value: function(t, e, n) {
                            var i = this.getRealCoordinates(t, e, n);
                            if (i.azimuth !== this.position.azimuth || i.elevation !== this.position.elevation || i.distance !== this.position.distance) switch (this.state) {
                                case "A":
                                    this.state = "A2B", this.pendingPosition = void 0, this._crossfadeTo("B", i);
                                    break;
                                case "B":
                                    this.state = "B2A", this.pendingPosition = void 0, this._crossfadeTo("A", i);
                                    break;
                                case "A2B":
                                    this.pendingPosition = i;
                                    break;
                                case "B2A":
                                    this.pendingPosition = i
                            }
                        }
                    }, {
                        key: "_crossfadeTo",
                        value: function(t, e) {
                            function n(t) {
                                t.audioContext.currentTime > r && (window.clearInterval(s), t.state = t.target, t.target = void 0, t.pendingPosition && t.setPosition(t.pendingPosition.azimuth, t.pendingPosition.elevation, t.pendingPosition.distance))
                            }
                            this.position = e, this.target = t;
                            var i = this.getHRTF(this.position.azimuth, this.position.elevation, this.position.distance),
                                o = this.audioContext.currentTime,
                                r = o + this.crossfadeDuration;
                            switch (this.target) {
                                case "A":
                                    this.convolverA.buffer = i, this.convolverB.gain.linearRampToValueAtTime(0, r), this.convolverA.gain.linearRampToValueAtTime(1, r);
                                    break;
                                case "B":
                                    this.convolverB.buffer = i, this.convolverA.gain.linearRampToValueAtTime(0, r), this.convolverB.gain.linearRampToValueAtTime(1, r)
                            }
                            var s = window.setInterval(n, 10, this)
                        }
                    }, {
                        key: "setCrossfadeDuration",
                        value: function(t) {
                            if (t) return this.crossfadeDuration = t / 1e3, this;
                            throw new Error("CrossfadeDuration setting error")
                        }
                    }, {
                        key: "getCrossfadeDuration",
                        value: function() {
                            return 1e3 * this.crossfadeDuration
                        }
                    }, {
                        key: "getPosition",
                        value: function() {
                            return this.position
                        }
                    }, {
                        key: "getHRTF",
                        value: function(t, e, n) {
                            var i = this.getNearestPoint(t, e, n);
                            return i.buffer
                        }
                    }, {
                        key: "sphericalToCartesian",
                        value: function(t, e, n) {
                            return {
                                x: n * Math.sin(t),
                                y: n * Math.cos(t),
                                z: n * Math.sin(e)
                            }
                        }
                    }, {
                        key: "getRealCoordinates",
                        value: function(t, e, n) {
                            var i = this.getNearestPoint(t, e, n);
                            return {
                                azimuth: i.azimuth,
                                elevation: i.elevation,
                                distance: i.distance
                            }
                        }
                    }, {
                        key: "getNearestPoint",
                        value: function(t, e, n) {
                            var i = t * Math.PI / 180,
                                o = e * Math.PI / 180,
                                r = this.sphericalToCartesian(i, o, n),
                                s = this.tree.nearest(r, 1)[0];
                            return s[0]
                        }
                    }, {
                        key: "HRTFDataset",
                        set: function(t) {
                            this.hrtfDataset = t, this.hrtfDatasetLength = this.hrtfDataset.length;
                            for (var e = 0; e < this.hrtfDatasetLength; e++) {
                                var n = this.hrtfDataset[e],
                                    i = n.azimuth * Math.PI / 180,
                                    o = n.elevation * Math.PI / 180,
                                    r = this.sphericalToCartesian(i, o, n.distance);
                                n.x = r.x, n.y = r.y, n.z = r.z
                            }
                            this.tree = a["default"].createKdTree(this.hrtfDataset, this.distance, ["x", "y", "z"])
                        },
                        get: function() {
                            return this.hrtfDataset
                        }
                    }]), t
                }();
            n["default"] = u;
            var l = function() {
                function t(e) {
                    o(this, t), this.audioContext = e.audioContext, this.gainNode = this.audioContext.createGain(), this.convNode = this.audioContext.createConvolver(), this.convNode.normalize = !1, this.gainNode.connect(this.convNode), this.oscillatorNode = this.audioContext.createOscillator(), this.gainOscillatorNode = this.audioContext.createGain(), this.oscillatorNode.connect(this.gainOscillatorNode), this.gainOscillatorNode.connect(this.gainNode), this.gainOscillatorNode.gain.setValueAtTime(0, this.audioContext.currentTime), this.oscillatorNode.start(0)
                }
                return i(t, [{
                    key: "connect",
                    value: function(t) {
                        return this.convNode.connect(t), this
                    }
                }, {
                    key: "disconnect",
                    value: function(t) {
                        return this.convNode.disconnect(t), this
                    }
                }, {
                    key: "input",
                    get: function() {
                        return this.gainNode
                    }
                }, {
                    key: "gain",
                    get: function() {
                        return this.gainNode.gain
                    }
                }, {
                    key: "buffer",
                    set: function(t) {
                        this.convNode.buffer = t
                    }
                }]), t
            }();
            e.exports = n["default"]
        }, {
            "babel-runtime/helpers/class-call-check": 4,
            "babel-runtime/helpers/create-class": 5,
            "babel-runtime/helpers/interop-require-default": 6,
            kdt: 9
        }],
        3: [function(t, e, n) {
            e.exports = {
                "default": t("core-js/library/fn/object/define-property"),
                __esModule: !0
            }
        }, {
            "core-js/library/fn/object/define-property": 7
        }],
        4: [function(t, e, n) {
            "use strict";
            n["default"] = function(t, e) {
                if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
            }, n.__esModule = !0
        }, {}],
        5: [function(t, e, n) {
            "use strict";
            var i = t("babel-runtime/core-js/object/define-property")["default"];
            n["default"] = function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var o = e[n];
                        o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), i(t, o.key, o)
                    }
                }
                return function(e, n, i) {
                    return n && t(e.prototype, n), i && t(e, i), e
                }
            }(), n.__esModule = !0
        }, {
            "babel-runtime/core-js/object/define-property": 3
        }],
        6: [function(t, e, n) {
            "use strict";
            n["default"] = function(t) {
                return t && t.__esModule ? t : {
                    "default": t
                }
            }, n.__esModule = !0
        }, {}],
        7: [function(t, e, n) {
            var i = t("../../modules/$");
            e.exports = function(t, e, n) {
                return i.setDesc(t, e, n)
            }
        }, {
            "../../modules/$": 8
        }],
        8: [function(t, e, n) {
            var i = Object;
            e.exports = {
                create: i.create,
                getProto: i.getPrototypeOf,
                isEnum: {}.propertyIsEnumerable,
                getDesc: i.getOwnPropertyDescriptor,
                setDesc: i.defineProperty,
                setDescs: i.defineProperties,
                getKeys: i.keys,
                getNames: i.getOwnPropertyNames,
                getSymbols: i.getOwnPropertySymbols,
                each: [].forEach
            }
        }, {}],
        9: [function(t, e, n) {
            function i(t, e, n) {
                this.obj = t, this.left = null, this.right = null, this.parent = n, this.dimension = e
            }

            function o(t, e, n) {
                function o(t, e, r) {
                    var s, a, u = e % n.length;
                    return 0 === t.length ? null : 1 === t.length ? new i(t[0], u, r) : (t.sort(function(t, e) {
                        return t[n[u]] - e[n[u]]
                    }), s = Math.floor(t.length / 2), a = new i(t[s], u, r), a.left = o(t.slice(0, s), e + 1, a), a.right = o(t.slice(s + 1), e + 1, a), a)
                }
                var s = this;
                this.root = o(t, 0, null), this.insert = function(t) {
                    function e(i, o) {
                        if (null === i) return o;
                        var r = n[i.dimension];
                        return t[r] < i.obj[r] ? e(i.left, i) : e(i.right, i)
                    }
                    var o, r, s = e(this.root, null);
                    return null === s ? void(this.root = new i(t, 0, null)) : (o = new i(t, (s.dimension + 1) % n.length, s), r = n[s.dimension], void(t[r] < s.obj[r] ? s.left = o : s.right = o))
                }, this.remove = function(t) {
                    function e(i) {
                        if (null === i) return null;
                        if (i.obj === t) return i;
                        var o = n[i.dimension];
                        return t[o] < i.obj[o] ? e(i.left, i) : e(i.right, i)
                    }

                    function i(t) {
                        function e(t, i) {
                            var o, r, s, a, u;
                            return null === t ? null : (o = n[i], t.dimension === i ? null !== t.right ? e(t.right, i) : t : (r = t.obj[o], s = e(t.left, i), a = e(t.right, i), u = t, null !== s && s.obj[o] > r && (u = s), null !== a && a.obj[o] > u.obj[o] && (u = a), u))
                        }

                        function o(t, e) {
                            var i, r, s, a, u;
                            return null === t ? null : (i = n[e], t.dimension === e ? null !== t.left ? o(t.left, e) : t : (r = t.obj[i], s = o(t.left, e), a = o(t.right, e), u = t, null !== s && s.obj[i] < r && (u = s), null !== a && a.obj[i] < u.obj[i] && (u = a), u))
                        }
                        var r, a, u;
                        return null === t.left && null === t.right ? null === t.parent ? void(s.root = null) : (u = n[t.parent.dimension], void(t.obj[u] < t.parent.obj[u] ? t.parent.left = null : t.parent.right = null)) : (r = null !== t.left ? e(t.left, t.dimension) : o(t.right, t.dimension), a = r.obj, i(r), void(t.obj = a))
                    }
                    var o;
                    o = e(s.root), null !== o && i(o)
                }, this.nearest = function(t, i, o) {
                    function a(o) {
                        function r(t, e) {
                            c.push([t, e]), c.size() > i && c.pop()
                        }
                        var s, u, l, h, f = n[o.dimension],
                            d = e(t, o.obj),
                            v = {};
                        for (h = 0; h < n.length; h += 1) h === o.dimension ? v[n[h]] = t[n[h]] : v[n[h]] = o.obj[n[h]];
                        return u = e(v, o.obj), null === o.right && null === o.left ? void((c.size() < i || d < c.peek()[1]) && r(o, d)) : (s = null === o.right ? o.left : null === o.left ? o.right : t[f] < o.obj[f] ? o.left : o.right, a(s), (c.size() < i || d < c.peek()[1]) && r(o, d), void((c.size() < i || Math.abs(u) < c.peek()[1]) && (l = s === o.left ? o.right : o.left, null !== l && a(l))))
                    }
                    var u, l, c;
                    if (c = new r(function(t) {
                            return -t[1]
                        }), o)
                        for (u = 0; i > u; u += 1) c.push([null, o]);
                    for (a(s.root), l = [], u = 0; i > u; u += 1) c.content[u][0] && l.push([c.content[u][0].obj, c.content[u][1]]);
                    return l
                }, this.balanceFactor = function() {
                    function t(e) {
                        return null === e ? 0 : Math.max(t(e.left), t(e.right)) + 1
                    }

                    function e(t) {
                        return null === t ? 0 : e(t.left) + e(t.right) + 1
                    }
                    return t(s.root) / (Math.log(e(s.root)) / Math.log(2))
                }
            }

            function r(t) {
                this.content = [], this.scoreFunction = t
            }
            r.prototype = {
                push: function(t) {
                    this.content.push(t), this.bubbleUp(this.content.length - 1)
                },
                pop: function() {
                    var t = this.content[0],
                        e = this.content.pop();
                    return this.content.length > 0 && (this.content[0] = e, this.sinkDown(0)), t
                },
                peek: function() {
                    return this.content[0]
                },
                remove: function(t) {
                    for (var e = this.content.length, n = 0; e > n; n++)
                        if (this.content[n] == t) {
                            var i = this.content.pop();
                            return void(n != e - 1 && (this.content[n] = i, this.scoreFunction(i) < this.scoreFunction(t) ? this.bubbleUp(n) : this.sinkDown(n)))
                        }
                    throw new Error("Node not found.")
                },
                size: function() {
                    return this.content.length
                },
                bubbleUp: function(t) {
                    for (var e = this.content[t]; t > 0;) {
                        var n = Math.floor((t + 1) / 2) - 1,
                            i = this.content[n];
                        if (!(this.scoreFunction(e) < this.scoreFunction(i))) break;
                        this.content[n] = e, this.content[t] = i, t = n
                    }
                },
                sinkDown: function(t) {
                    for (var e = this.content.length, n = this.content[t], i = this.scoreFunction(n);;) {
                        var o = 2 * (t + 1),
                            r = o - 1,
                            s = null;
                        if (e > r) {
                            var a = this.content[r],
                                u = this.scoreFunction(a);
                            i > u && (s = r)
                        }
                        if (e > o) {
                            var l = this.content[o],
                                c = this.scoreFunction(l);
                            (null == s ? i : u) > c && (s = o)
                        }
                        if (null == s) break;
                        this.content[t] = this.content[s], this.content[s] = n, t = s
                    }
                }
            }, e.exports = {
                createKdTree: function(t, e, n) {
                    return new o(t, e, n)
                }
            }
        }, {}]
    }, {}, [1])(1)
});
