// Web Wall Whispers
//
// Interactive web-based audio-visual installation.
// Developed for Fondazione Spinola-Banna per l'Arte.
// Copyright (C) 2018 Francesco Cretti
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see
// https://bitbucket.org/splsteam/splswebapp/src/master/LICENSE.txt.
//
// JS developer - francesco.cretti@gmail.com
// Coordinator - leehooni@gmail.com
//
// Home Page: www.webwallwhispers.net
// Bug report: https://bitbucket.org/splsteam/splswebapp/issues

Array.prototype.diff = function (a) {
    return this.filter(function (i) {
        return a.indexOf(i) === -1;
    });
};


Array.prototype.uniqueArray = function () {
  return this.filter(function(obj, pos, arr) {
    return arr.indexOf(obj) == pos;
  });
};

var WebWallWhispers = WebWallWhispers === undefined ? {} : WebWallWhispers;

(function (W) {
  'use strict';

  /* **********
   * PROPERTIES
   * **********/
  // Audio
  W.wallSounds = [];
  W.actionSounds = [];
  // Stream attractors
  W.streamAttractors = [];
  W.playingStreams = [];
  W.insideAttractors = [];
  W.playAreaRatios = [];
  // Viewer
  W.img = '';
  // DOM
  W.splash = document.getElementById('splash');
  W.audioLog = document.getElementById('audioLogs');
  // Reverberator
  W.reverberator = '';
  // Status
  W.status = {
    system: { },
    audio: {
      appMuted: false,
      ASfinishLoad: false,
      WSfinishLoad: false,
      RSfinishLoad: false,
      OSDfinishLoad: false
    },
    viewer: {
      prevZoom: 0,
      layerFirstLand: [true, true, true, true, true, true]
    },
    interaction: {
      lastActionTime: Date.now(),
      lastPinchTime: Date.now(),
      pinchSleepTime: 2000,
      pinchZooming: false,
      lastKeyZoomTime: Date.now(),
      keyZoomSleepTime: 2000,
      insideApp: false,
      stbyCheckId: null,
      panCount: 0,
      panSleep: false
    }
  };

  /* **********
   * FUNCTIONS
   * **********/

  // ******** INIT FUNCTION ********
  W.onLoad = function (e) {
    W.getJSON('app/config.json', W.init);
  };

  W.init = function (err, config) {
    if (err !== null) {
      console.error('Error in loading configuration file: ' + err);
    } else {
      // Init app with loaded config
      console.log('JSON Configuration loaded');
      console.log('Version 0.3.0 - Apr 2018');
      W.getSystemSpecs();
      if (W.status.system.allowed) {
        if (W.status.system.lite || !Hls.isSupported()) {
          W.setUpLiteApplication(config);
          document.dispatchEvent(new CustomEvent('lite-version'));
        } else {
          W.setUpApplication(config);
          W.status.system.lite = false;
        }
      } else {
        document.dispatchEvent(new CustomEvent('browser-block'));
      }
    }
  };


  W.getSystemSpecs = function () {
    W.status.system.allowed = W.checkDevice.isAllowed();
    W.status.system.lite = W.checkDevice.liteVersion();
    W.status.system.browser = W.checkDevice.getBrowserSpecs();
    W.status.system.mobile = W.checkDevice.isMobile.any();
    W.status.system.iOS = W.checkDevice.isMobile.iOS();
    W.status.system.android = W.checkDevice.isMobile.Android();
    W.status.system.safari = W.checkDevice.isSafari();
    var screenSize = W.checkDevice.getScreenSize();
    W.status.system.screenWidth = screenSize.width;
    W.status.system.screenHeight = screenSize.height;
    W.status.system.screenArea = screenSize.area;
    W.status.system.aspectRatio = W.checkDevice.getAspectRatio();
    var d;
    if (W.status.system.android) {
      d = 'android';
    } else if (W.status.system.iOS) {
      d = 'iOS';
    } else if (W.status.system.mobile) {
      d = 'mobile';
    } else {
      d = 'desktop';
    }
    console.log(`Device: ${d} | Browser: ${W.status.system.browser.name} ${W.status.system.browser.version}`);
    console.log(`Aspect ratio: ${W.status.system.aspectRatio},
      Resolution: ${W.status.system.screenWidth}x${W.status.system.screenHeight},
      Screen area: ${W.status.system.screenArea}px`);
    // TODO check if browser needs lite or full version
  };

  W.enter = function () {
    if (W.ASfinishLoad && W.WSfinishLoad && W.OSDfinishLoad) {
      // Welcome message
      if (W.status.system.lite) {
        console.log('%c** Welcome to WebWallWhispers App  - LITE VERSION **', 'background:lightgreen; color:grey');
      } else {
        console.log('%c** Welcome to WebWallWhispers App **', 'background:darkgreen; color:white');
      }
      // Resume audio context -> https://goo.gl/7K7WLu
      W.status.audio.context.resume().then(() => {
        console.log('Playback resumed successfully');
      });
      // Go to layer 0
      W.img.goHome();
      // Remove splash screen
      W.splash.parentNode.removeChild(W.splash);
      // Check user activity every 3 seconds
      W.status.interaction.stbyCheckId = setInterval(W.checkUserInactivity, 3000);
      // For mobile devices
      if (W.status.system.mobile && !W.status.system.lite) {
        for (var i = 0; i < W.wallSounds.length; i++) {
          W.wallSounds[i].audioElement.play();
          W.wallSounds[i].audioElement.pause();
        }
      }
      // Prevent default browser zooming on spread gesture
      document.addEventListener('touchstart', function (e) {
          if (e.touches.length === 2) {
            e.preventDefault();
          }
      }, {passive: false});
      // Fade in standy sound
      W.standBySound.fadeIn(3, true);
      // Play aleatory sound 30sec sprite every 3 min
      if (!W.status.system.lite) {
        W.status.audio.aleatoryInterval = setInterval(W.playAleatorySound, 180000);
      }
      //Set tru inside app flag
      W.status.interaction.insideApp = true;
    }
  };

  // *************************************************************************/
  // *************************************************************************/
  // **************************** DESKTOP APP ********************************

  // ******** USER ACTIONS ***********

  W.manageNavigatorClick = function (e) {
    W.anyUserAction();
    var z = W.img.getZoomLevel(true);
    switch (z) {
      case 2:
        var si = W.randomInt(0, 9);
        W.actionSounds[si].fire();
        break;
      case 3:
        var si = W.randomInt(0, 19);
        W.actionSounds[si].fire();
        break;
      case 4:
        var si = W.randomInt(10, 19);
        W.actionSounds[si].fire();
        break;
      case 5:
        var si = W.randomInt(20, 25);
        W.actionSounds[si].fire();
        break;
    }
  };

  W.manageScroll = function (e) {
    if (W.status.interaction.insideApp) {
      W.anyUserAction();
      e = e || window.event;
      W.dLog && console.log(e);
      var z = W.img.getZoomLevel(true);
      if (e.originalEvent.deltaX === 0) {
        switch (z) {
          case 1:
            if (e.scroll < 0) {
              // W.img.scrollZoomToCenter(e);
              W.img.goHome();
            } else {
              W.img.scrollZoomToPointer(e);
            }
            break;
          case 2:
            if (e.scroll < 0) {
              W.img.scrollZoomToCenter(e);
            } else {
              W.img.scrollZoomToPointer(e);
            }
            break;
          case 3:
            W.img.scrollZoomToPointer(e);
            break;
          case 4:
            W.img.scrollZoomToPointer(e);
            break;
          case 5:
            W.img.scrollZoomToPointer(e);
            break;
          default:
            // no scroll to zoom
        }
      }
    }
  };

  W.manageKeyboard = function (e) {
    if (W.status.interaction.insideApp) {
      W.anyUserAction();
      var thisKeyInteractionTime = Date.now();
      var deltaKeyTime = thisKeyInteractionTime - W.status.interaction.lastKeyZoomTime;
      e.preventDefaultAction = false;
      if (W.img.getZoomLevel() === 0) {
        e.preventVerticalPan = true;
        e.preventHorizontalPan = true;
      }
      if (W.img.getZoomLevel() === 1) {
        e.preventVerticalPan = true;
        e.preventHorizontalPan = false;
      }
      if (deltaKeyTime > W.status.interaction.keyZoomSleepTime && W.isZoomKey(e)) {
        W.status.interaction.lastKeyZoomTime = thisKeyInteractionTime;
      } else if (deltaKeyTime > 50 && W.isZoomKey(e)) {
        // if delta > 50 -> user double action, otherwise double event fired
        e.preventDefaultAction = true;
      }
    } else {
      e.preventDefaultAction = true;
    }
  };

  W.isZoomKey = function (e) {
    switch (e.keyCode) {
      case 43: //=|+
      case 187: //=|+
      case 61: //=|+
      case 45: //-|_
      case 189: //-|_
      case 48: //0|) - goHome
        return true;
      case 119: //w
      case 87: //w
      case 115: //s
      case 83: //s
      case 38: //up arrow
      case 40: //down arrow
        if (e.shift) {
          return true;
        } else {
          return false;
        }
      default:
        return false;
    }
  };

  W.manageDoubleTap = function (e) {
    W.anyUserAction();
    e.preventDefaultAction = true;
    W.dLog && console.log('double click');
  };

  W.manageTap = function (e) {
    if (W.status.interaction.insideApp) {
      W.anyUserAction();
      var z = W.img.getZoomLevel(true);
      var posY = W.img.getCenter().y;
      var posX = W.img.viewer.viewport.pointFromPixel(e.position).x;
      var targetHorizontalCenter = new OpenSeadragon.Point(posX, posY);
      var leftBound = W.img.viewer.viewport.getHomeBounds().x;
      var rightBound = leftBound + W.img.viewer.viewport.getHomeBounds().width;
      var eventY = W.img.viewer.viewport.pointFromPixel(e.position).y;
      var targetCenter = new OpenSeadragon.Point(posX, eventY);
      switch (z) {
        case 0:
          if (e.quick) {
            W.img.doSingleZoomIn();
            W.img.panTo(targetHorizontalCenter);
            // TODO add action sound
          }
          break;
        case 1:
          if (e.quick && posX > leftBound && posX < rightBound) {
            W.img.panTo(targetHorizontalCenter);
          }
          break;
        case 2:
          if (e.quick && posX > leftBound && posX < rightBound) {
            W.img.panTo(targetCenter);
            var si = W.randomInt(0, 9);
            W.actionSounds[si].fire();
          }
          break;
        case 3:
          if (e.quick && posX > leftBound && posX < rightBound) {
            W.img.panTo(targetCenter);
            var si = W.randomInt(0, 19);
            W.actionSounds[si].fire();
          }
          break;
        case 4:
          if (e.quick && posX > leftBound && posX < rightBound) {
            W.img.panTo(targetCenter);
            var si = W.randomInt(10, 19);
            W.actionSounds[si].fire();
          }
          break;
        case 5:
          if (e.quick && posX > leftBound && posX < rightBound) {
            W.img.panTo(targetCenter);
            var si = W.randomInt(20, 25);
            W.actionSounds[si].fire();
          }
          break;
        }
      }
    };

  W.anyUserAction = function () {
    if (W.img.autoPilot.active) {
      W.img.autoPilotExit();
    }
  };

  // ************ OSD ACTIONS *************

  W.manageAnimationStart = function () {
    W.status.viewer.zoomStartLevel = W.img.getZoomLevel(true);
    W.dLog && console.log(`%canimation start ${W.status.viewer.zoomStartLevel}`, 'background:darkgreen; color:white');
  };

  W.updateViewer = function () {
    var z = W.img.getZoomLevel(false);
    W.img.drawPlayArea(W.playAreaRatios[z], W.streamAttractors[z]);
  };

  W.manageZoom = function (e) {
    if (W.status.interaction.insideApp) {
      var ez = e.zoom;
      var deltaZ = Math.abs(W.status.viewer.prevZoom - e.zoom);
      var zl = W.img.getMatchedZoomLevelIndex(ez);
      if (zl > W.status.viewer.prevZoom && (deltaZ > 1e-10 || isNaN(deltaZ))) {
        W.dLog && console.log(`%czoomin in | event zoom: ${ez} | zoom level ${zl}`, 'background:beige; color:darkblue');
        if (!W.status.system.lite) {
          W.setAudioEffects(zl);
        }
        W.setUserGestures(zl);
        W.actionSounds[25+zl].fadeIn(1); //zoom in sound
        W.zoomSound = W.actionSounds[25+zl];
        if (zl === 1) {
          W.img.alignVertically();
        }
      } else if (zl < W.status.viewer.prevZoom && (deltaZ > 1e-10 || isNaN(deltaZ))) {
        W.dLog && console.log(`%czoomin out | event zoom: ${ez} | zoom level ${zl}`, 'background:beige; color:darkblue');
        if (!W.status.system.lite) {
          W.setAudioEffects(zl);
        }
        W.setUserGestures(zl);
        W.actionSounds[31+zl].fadeIn(1); //zoom out sound
        W.zoomSound = W.actionSounds[31+zl];
        if (zl === 1) {
          W.img.alignVertically();
        }
      }
      W.status.viewer.prevZoom = zl;
    }
  };

  W.updateStandby = function () {
    if (typeof W.standBySound !== 'undefined' && W.status.interaction.insideApp) {
      // Update time
      W.status.interaction.lastActionTime = Date.now();
      // Fade out standby sound
      if (!W.standBySound.isFadingOut()) {
        W.standBySound.fadeOut(5);
      }
      if (W.status.interaction.stbyCheckId === null) {
        // Set interval
        W.status.interaction.stbyCheckId = setInterval(W.checkUserInactivity, 3000);
        setTimeout(W.setStandbyVolumes, 1000, false); // false for exit
        W.dLog && console.log('STANDBY EXIT');
        document.dispatchEvent(new CustomEvent('standby-exit'));
      }
    }
  };

  W.playAleatorySound = function () {
    // if not in standby mode
    if (W.status.interaction.stbyCheckId !== null) {
      var start = W.randomInt(0, W.aleatorySound.getDuration());
      var stop = start + 30;
      W.dLog && console.log(`Starting aleatory sound from ${start} s`);
      W.aleatorySound.playSprite(start, stop, false);
    }
  };

  W.checkUserInactivity = function () {
    var t = Date.now();
    var timeOccurred = (t - W.status.interaction.lastActionTime) / 1000;
    if (timeOccurred > 120 && !W.img.autoPilot.active) {
      W.dLog && console.log(`Inactivity time: ${timeOccurred} secs.`);
      W.standBySound.fadeIn(3, true);
      W.setStandbyVolumes(true); // true for enter
      W.dLog && console.log('STANDBY ENTER');
      document.dispatchEvent(new CustomEvent('standby-enter'));
      clearInterval(W.status.interaction.stbyCheckId);
      W.status.interaction.stbyCheckId = null;
    }
  };

  W.setStandbyVolumes = function (enter) {
    W.dLog && console.log(`setting stanby volumes enter: ${enter}`);
    var delta;
    if (enter) {
      delta = -0.6;
    } else {
      delta = 0.6;
    }
    W.checkPlayingAudios().forEach(function (el) {
      if (el !== 21) { // if is not standby sound
        var newGain = W.wallSounds[el].getMasterGain() + delta;
        if (newGain > 0) {
          W.wallSounds[el].fadeToGain(newGain, 3);
        }
      } else {
      }
    });
  };

  W.managePan = function (e) {
    if (W.status.interaction.insideApp) {
      if (!W.status.interaction.panSleep) {
        // Get current view center
        var center = W.img.getCenter();
        var z = W.img.getZoomLevel(false);
        switch (z) {
          case 1:
          W.layerPan(center, z, {
            calcAzimuth: true,
            constA: 0.02,
            calcFreq: true,
            constF: 0.01832,
            expF: 2.5,
            calcGain: true,
            expG: 25
          });
            break;
          case 2:
          W.layerPan(center, z, {
            calcAzimuth: true,
            constA: 0.02,
            calcFreq: false,
            calcGain: true,
            expG: 16
          });
            break;
          case 3:
          W.layerPan(center, z, {
            calcAzimuth: true,
            constA: 0.005,
            calcFreq: true,
            constF: 0.0821,
            expF: 3.2,
            calcGain: true,
            expG: 100
          });
            break;
          case 4:
          W.layerPan(center, z, {
            calcAzimuth: false,
            calcFreq: false,
            calcGain: true,
            expG: 100
          });
            break;
          case 5:
            W.layer5Pan();
            break;
          default:
        }
        W.status.interaction.panCount++;
        W.status.interaction.panSleep = true;
        setTimeout(function () {
          W.status.interaction.panSleep = false;
        }, 750);
        W.dLog && console.log(`Pan count: ${W.status.interaction.panCount}`);
        if (W.status.interaction.panCount > 4) {
          W.status.interaction.panCount = 0;
          W.img.viewer.raiseEvent('animation-finish');
          W.dLog && console.log(`Pan events limit reached. Raising animation finish event`);
        }
      }
    }
  };

  W.layerPan = function (center, z, options) {
    // Default values
    let defaults = {
      calcAzimuth: true,
      constA: 0.02,
      calcFreq: true,
      constF: 0.01832,
      expF: 2.5,
      calcGain: true,
      expG: 25
    };
    let actual = Object.assign({}, defaults, options);
    // For each attractor inside PA
    W.insideAttractors.forEach(function(el) {
      if (!el.omni) {
        // Create OSD point object
        var attractor = new OpenSeadragon.Point(el.x, el.y);
        // Calc distance
        var distance = center.distanceTo(attractor);
        // **** BINAURAL *****
        var azimuth;
        if (actual.calcAzimuth) {
          var c1 = center.x - el.x;
          var c2 = center.y - el.y;
          var r2 = Math.pow(c1, 2) + Math.pow(c2, 2);
          // Clip values to avoid abrupt passage left/right if close to attr. point
          var K2 = Math.pow(actual.constA, 2); // Clipping area const = constA^2
          if (r2 < K2) {
            c2 = W.sign(c2) * Math.sqrt(K2 - Math.pow(c1, 2));
            W.dLog && console.log(`Clipping c2...`);
          }
          // Calc azimuth
          azimuth = W.degrees(Math.atan2(c1, -c2));
          // Get old position
          var oldPos = W.wallSounds[el.stream].binauralizator.getPosition();
          // Assign to binaural node
          W.wallSounds[el.stream].binauralizator.interpolatePosition(oldPos, -azimuth);
        } else {
          azimuth = undefined;
        }
        // **** LOW PASS FILTER ****
        var freq;
        if (actual.calcFreq) {
          // var KF = 0.0821; // --> so when dist = 0 freq = 22 kHz
          // var KF = 0.03568; // --> so when dist = 0 freq = 22 kHz
          // var KF = 0.01832; // --> so when dist = 0 freq = 22 kHz
          // Calc frequency
          freq = 1 / Math.pow(actual.constF + distance, actual.expF);
          // Assign to LPF cut frequency. Force if fading
          W.wallSounds[el.stream].filter.fadeToLPFrequency(freq, 1);
        } else {
          freq = undefined;
        }
        // **** DRY GAIN *****
        var gain;
        if (actual.calcGain) {
          // Calc gain
          gain = 1 / Math.pow(1 + distance, actual.expG);
          // Assign to dry gain
          W.wallSounds[el.stream].filter.fadeToDryGain(gain, 1);
        } else {
          gain = undefined;
        }
        W.dLog && console.log(`L${z} - Stream: ${el.stream} | Dist: ${distance} | LPFf: ${freq} | Gain: ${gain} | Azimuth: ${azimuth}`);
      }
    });
  };

  W.layer5Pan = function () {
    var increment = 10; // degrees
    W.streamAttractors[5].forEach(function (el) {
      var gain = W.wallSounds[el.stream].getMasterGain();
      var oldPos = W.wallSounds[el.stream].binauralizator.getPosition();
      var newPos = (oldPos + increment * gain) % 360;
      W.wallSounds[el.stream].binauralizator.interpolatePosition(oldPos, newPos);
      W.dLog && console.log(`L5 - Stream: ${el.stream} | OldPos: ${oldPos} | NewPos ${newPos}`);
    });
  };

  W.manageAnimationFinish = function (e) {
    if (W.status.interaction.insideApp) {
      var center = W.img.getCenter();
      var x = center.x.toFixed(3);
      var y = center.y.toFixed(3);
      var zv = W.img.getZoomValue(false);
      var z = W.img.getZoomLevel(false);
      W.status.interaction.pinchZooming = false;
      W.zoomSound.fadeOut(1);
      if (z !== 0) {
        W.updateStandby();
      }
      W.dLog && console.log(`Animation end at: ${x}, ${y}, ${zv} -> ${z}`);
      switch (z) {
        case 0:
          W.layerAnimation(center, z, false);
          W.standBySound.fadeIn(3, true);
          break;
        case 1:
          W.layerAnimation(center, z, true);
          break;
        case 2:
        case 3:
          setTimeout(W.layerAnimation, 200, center, z, true);
          break;
        case 4:
          setTimeout(W.layerAnimation, 200, center, z, false);
          break;
        case 5:
          setTimeout(W.layer5Animation, 200, z);
          break;
        default:
          W.dLog && console.warn(`Missing animation logic for zoom ${z}`);
      }
      W.img.viewer.raiseEvent('pan');
    }
  };

  W.setUserGestures = function (z) {
    switch (z) {
      case 0:
        W.img.viewer.panVertical = false;
        W.img.viewer.panHorizontal = false;
        W.img.viewer.navigator.panVertical = false;
        W.img.viewer.navigator.panHorizontal = false;
        break;
      case 1:
        W.img.viewer.panVertical = false;
        W.img.viewer.panHorizontal = true;
        W.img.viewer.navigator.panVertical = false;
        W.img.viewer.navigator.panHorizontal = true;
        break;
      default:
        W.img.viewer.panVertical = true;
        W.img.viewer.panHorizontal = true;
        W.img.viewer.navigator.panVertical = true;
        W.img.viewer.navigator.panHorizontal = true;
    }
  };

  W.layerAnimation = function (center, z, random) {
    W.playAreaTransition(center, z, random);
    if (z !== W.status.viewer.zoomStartLevel || z === 0) {
      if (W.status.viewer.layerFirstLand[z]) {
        W.raiseLandingEvent(z, true);
        W.status.viewer.layerFirstLand[z] = false;
      } else {
        W.raiseLandingEvent(z, false);
      }
      if (W.status.viewer.zoomStartLevel === 5) {
        W.fadeOutLayer5();
      }
      W.dLog && console.log(`%cLanded on layer ${z}`, 'background:red; color:black');
    }
  };

  W.layer5Animation = function (z) {
    W.playAreaTransition(0, z, false);
    var colorsGain = W.img.getColorsWeight();
    W.dLog && console.log(`Colors gain: ${colorsGain}`);
    // Divide circumference for equidistant binaural positioning
    var shift = 360 / W.streamAttractors[z].length;
    var offset = 45;
    // If landed from another layer
    if (z !== W.status.viewer.zoomStartLevel) {
      if (W.status.viewer.layerFirstLand[z]) {
        W.raiseLandingEvent(z, true);
        W.status.viewer.layerFirstLand[z] = false;
      } else {
        W.raiseLandingEvent(z, false);
      }
      W.dLog && console.log(`%cLanded on layer ${z}`, 'background:red; color:black');
      // For each stream in layer 5
      W.streamAttractors[z].forEach(function (el, idx) {
        if (!W.wallSounds[el.stream].isPlaying()) {
          // Set binaural position
          if (!W.status.system.mobile) {
            W.wallSounds[el.stream].binauralizator.setPosition(offset + shift * idx);
          }
          // Play silently
          W.wallSounds[el.stream].setMasterGain(0);
          W.wallSounds[el.stream].play();
        }
      });
    }
    // Adjust gain
    W.streamAttractors[z].forEach(function (el, idx) {
      W.wallSounds[el.stream].fadeToGain(colorsGain[idx], 3);
    });
  };

  W.fadeOutLayer5 = function () {
    W.streamAttractors[5].forEach(function (el, idx) {
      if (!W.wallSounds[el.stream].isFadingOut()) {
        W.wallSounds[el.stream].fadeOut(5);
      }
    });
  };

  // ********* CALCS
  /**
   * @function findAttractorsInPlayArea
   * @param {Number} playAreaRatio play area ratio
   * @param {Array} attractors array of attranctors
   * @param {Object} center OpenSeadragon.Point object - center of the view
   **/
  W.findAttractorsInPlayArea = function (playAreaRatio, attractors, center) {
    W.dLog && console.log(`Current play area ratio: ${playAreaRatio}`);
    var insideStreams = [];
    var insideAttr = [];
    for (let i = 0; i < attractors.length; i++) {
      var attrPoint = new OpenSeadragon.Point(attractors[i].x, attractors[i].y);
      var newdistance = center.distanceTo(attrPoint) / attractors[i].coeff;
      // W.dLog && console.log(`center index: ${i} | distance: ${newdistance}`);
      if (newdistance < playAreaRatio) {
        W.dLog && console.log(`Attractor ${i} is inside the playing area`);
        // Push stream inside array
        insideStreams.push(attractors[i]);
        var obj = {
          attr: attractors[i],
          dist: newdistance
        }
        insideAttr.push(obj);
      }
    }
    W.insideAttractors = W.uniqueClosestStreamsArray(insideAttr);
    return insideStreams;
  };

  /**
   * @function playAreaTransition
   * @param {Object} center
   * @param {Number} z
   * @param {Boolean} random whether do random seek before fade in
   **/
  W.playAreaTransition = function (center, z, random) {
    var insideStreams = [];
    var incomingStreams = [];
    var outgoingStreams = [];
    // Get correct attractors array
    if (z !== 5) {
      var attractors = W.streamAttractors[z];
      // Find streams inside the playing area
      insideStreams = W.findAttractorsInPlayArea(W.playAreaRatios[z], attractors, center);
      // Silence sound
      if (insideStreams.length === 0 && z !== 0) {
        W.dLog && console.log('playing silence sound');
        W.silenceSound.fadeIn(5);
      } else if (W.silenceSound.isPlaying() && !W.silenceSound.isFading()) {
        W.dLog && console.log('stopping silence sound');
        W.silenceSound.fadeOut(5);
      }
      W.dLog && console.log(`Inside PA ${insideStreams.length} streams`);
      // Get streams gone out
      outgoingStreams = W.streamDiff(W.playingStreams, insideStreams);
      // Remove duplicates and NaNs
      outgoingStreams = W.uniqueStreamArray(outgoingStreams);
      // Get streams came in
      incomingStreams = W.streamDiff(insideStreams, W.playingStreams);
      // Remove duplicates and NaNs
      incomingStreams = W.uniqueStreamArray(incomingStreams);
    } else {
      W.dLog && console.log(`Layer 5 play area transition`);
      // Fade everything out
      outgoingStreams = W.playingStreams;
    }
    setTimeout(W.manageOutgoingSounds, 200, outgoingStreams);
    W.manageIncomingSounds(incomingStreams, random);
    // Update current playing streams
    W.playingStreams = insideStreams;
  };

  /**
   * @function manageIncomingSounds
   * @param {Object} incoming array of stream attractors coming inside the play area
   * @param {Boolean} random whether do random seek before fade in
   **/
  W.manageIncomingSounds = function (incoming, random) {
    // For each stream...
    incoming.forEach(function (el, idx) {
      W.dLog && console.log(`%cincoming elements ${el.stream}`, 'color:coral');
        // Check if not already fading
        if (!W.wallSounds[el.stream].isFading()) {
          if (el.sprite) {
            // Play correct sprite
            W.wallSounds[el.stream].playSprite(el.start, el.stop);
            W.dLog && console.log(`Sprite: Stream ${el.stream} [${el.start} - ${el.stop}]`);
          } else {
            // Fade in
            if (el.markers !== undefined) {
              var markerTime = el.markers[W.randomInt(0, el.markers.length - 1)];
            }
            W.dLog && console.log(`Marker choosen: ${markerTime}`);
            setTimeout(W.wallSounds[el.stream].fadeIn.bind(W.wallSounds[el.stream], 2.5, random, markerTime), 100 * idx);
          }
          // Schedule to check if user has moved during fade
          setTimeout(function () {
            W.reCheckOutgoingStatus(el);
          }, 2600 + 100 * idx);
          W.dLog && console.log(`Stream ${el.stream} fading in`);
        } else {
          W.dLog && console.log(`Stream ${el.stream} is already fading`);
        }
      });
  };

  /**
   * @function manageOutgoingSounds
   * @param {Object} outgoing array of stream attractors going outside the play area
   **/
  W.manageOutgoingSounds = function (outgoing) {
    // For each stream...
    outgoing.forEach(function (el, idx) {
      W.dLog && console.log(`%coutgoing elements ${el.stream}`, 'color:coral');
      // Check if not already fading
      if (!W.wallSounds[el.stream].isFading()) {
        // Fade out
        setTimeout(W.wallSounds[el.stream].fadeOut.bind(W.wallSounds[el.stream], 5), 100 * idx);
        // Schedule to check if user has moved during fade
        setTimeout(function () {
          W.reCheckIncomingStatus(el);
        }, 5100 + 100 * idx);
        W.dLog && console.log(`Stream ${el.stream} fading out`);
      } else {
        W.dLog && console.log(`Stream ${el.stream} is already fading`);
      }
    });
  };

  /**
   * @function reCheckIncomingStatus Control function for user movements while sounds are fading
   * @param {Object} streamAttr stream attractor object
   **/
  W.reCheckIncomingStatus = function (streamAttr) {
    W.dLog && console.log(`Re checking incoming status fo stream ${streamAttr.stream}`);
    // Force remove stream from playing streams (if present)
    W.playingStreams = W.removeStreamAttractor(W.playingStreams, streamAttr);
    // Update zoom start level
    // W.img.viewer.raiseEvent('animation-start');
    // Raise event 'animation finish' to check again
    W.img.viewer.raiseEvent('animation-finish');
  };

  /**
   * @function reCheckOutgoingStatus Control function for user movements while sounds are fading
   * @param {Object} streamAttr stream attractor object
   **/
  W.reCheckOutgoingStatus = function (streamAttr) {
    W.dLog && console.log(`Re checking outgoing status fo stream ${streamAttr.stream}`);
    // Force push stream in playing streams
    W.playingStreams.push(streamAttr);
    // Raise event 'animation finish' to check again
    W.img.viewer.raiseEvent('animation-finish');
  };

  /**
   * @function setAudioEffects prepare audio effects
   * @param {Number} layer layer number
   **/
  W.setAudioEffects = function (layer) {
    var effectOptions,
      effectOptionsUpper,
      effectOptionsLower,
      effectOptionsOff;
    switch (layer) {
      case 1:
        effectOptions = {
          lpFilter: true,
          lpFreq: 22000,
          reverb: true,
          wetGain: 0.25,
          binaural: true
        };
        break;
      case 2:
        effectOptions = {
          lpFilter: false,
          reverb: false,
          binaural: false
        };
        effectOptionsUpper = {
          lpFilter: true,
          lpFreq: 800,
          reverb: false,
          distortion: true,
          distAmount: 5,
          binaural: true
        };
        effectOptionsLower = {
          randomizeGain: true,
          lpFilter: false,
          hpFilter: true,
          hpFreq: 8000,
          reverb: false,
          distortion: true,
          distAmount: 10,
          binaural: true
        };
        break;
      case 3:
        effectOptions = {
          lpFilter: true,
          lpFreq: 22000,
          reverb: true,
          wetGain: 0.6,
          binaural: true
        };
        break;
      case 4:
        effectOptions = {
          lpFilter: false,
          reverb: false,
          binaural: false
        };
        effectOptionsUpper = {
          lpFilter: true,
          lpFreq: 5000,
          distortion: false,
          reverb: true,
          wetGain: 1,
          binaural: false
        };
        break;
      case 5:
        effectOptions = {
          lpFilter: false,
          reverb: false,
          binaural: true
        };
        break;
    }
    effectOptionsOff = {
      lpFilter: false,
      distortion: false,
      hpFilter: false,
      reverb: false,
      binaural: false
    };
    // Effect initialization
    if (layer !== 0) {
      var atttractors = W.uniqueStreamArray(W.streamAttractors[layer]);
      atttractors.forEach(function (el) {
        if (el.fromLayer === "upper") {
          W.wallSounds[el.stream].setEffectChain(effectOptionsUpper, W.reverberator);
          W.dLog && console.log(`Effect chain init for UPPER stream ${el.stream}`);
        } else if (el.fromLayer === "lower") {
          W.wallSounds[el.stream].setEffectChain(effectOptionsLower, W.reverberator);
          W.dLog && console.log(`Effect chain init for LOWER stream ${el.stream}`);
        } else {
          W.wallSounds[el.stream].setEffectChain(effectOptions, W.reverberator);
          W.dLog && console.log(`Effect chain init for CURRENT LAYER stream ${el.stream}`);
        }
      });
    }
  };

  // *************************************************************************/
  // *************************************************************************/
  // **************************** LITE APP ********************************

  W.manageAnimationFinishLite = function (e) {
    if (W.status.interaction.insideApp) {
      var center = W.img.getCenter();
      var x = center.x.toFixed(3);
      var y = center.y.toFixed(3);
      var zv = W.img.getZoomValue(false);
      var z = W.img.getZoomLevel(false);
      W.status.interaction.pinchZooming = false;
      W.zoomSound.fadeOut(1);
      if (z !== 0) {
        W.updateStandby();
      }
      W.dLog && console.log(`Animation end at: ${x}, ${y}, ${zv} -> ${z}`);
      W.liteTransition(z);
      W.img.viewer.raiseEvent('pan');
    }
  };

  W.liteTransition = function (z) {
    if (!W.wallSounds[z].isFadingIn()) {
      W.dLog && console.log(`Fade in sounds ${z}`);
      // Fade in
      W.wallSounds[z].fadeIn(2.5, true);
    }
    if (z !== W.status.viewer.zoomStartLevel || z === 0) {
      W.checkPlayingAudios().forEach(function (el) {
        if (el !== z && el !== 0 && !W.wallSounds[el].isFadingOut()) {
          W.dLog && console.log('FADING OUT '+ el);
          W.wallSounds[el].fadeOut(6);
        }
      });
      if (W.status.viewer.layerFirstLand[z]) {
        W.raiseLandingEvent(z, true);
        W.status.viewer.layerFirstLand[z] = false;
      } else {
        W.raiseLandingEvent(z, false);
      }
      W.dLog && console.log(`%cLanded on layer ${z}`, 'background:red; color:black');
    }
  };

  // ************************************************************************/
  // *********************** MOBILE VERSION *********************************/
  // ************************************************************************/

  W.manageMobileZoom = function (e) {
    if (W.status.interaction.insideApp) {
      var ez = e.zoom;
      var deltaZ = Math.abs(W.status.viewer.prevZoom - e.zoom);
      var zl = W.img.getMatchedZoomLevelIndex(ez);
      if (zl > W.status.viewer.prevZoom && (deltaZ > 1e-10 || isNaN(deltaZ))) {
        W.dLog && console.log(`%czoomin in | event zoom: ${ez} | zoom level ${zl}`, 'background:beige; color:darkblue');
        // W.setMobileAudioEffects(zl);
        W.setMobileGestures(zl);
        W.actionSounds[25+zl].fadeIn(1); //zoom in sound
        W.zoomSound = W.actionSounds[25+zl];
        if (zl === 1) {
          W.img.alignVertically();
        }
      } else if (zl < W.status.viewer.prevZoom && (deltaZ > 1e-10 || isNaN(deltaZ))) {
        W.dLog && console.log(`%czoomin out | event zoom: ${ez} | zoom level ${zl}`, 'background:beige; color:darkblue');
        // W.setMobileAudioEffects(zl);
        W.setMobileGestures(zl);
        W.actionSounds[31+zl].fadeIn(1); //zoom in sound
        W.zoomSound = W.actionSounds[31+zl];
        if (zl === 1) {
          W.img.alignVertically();
        }
      }
      W.status.viewer.prevZoom = zl;
    }
  };

  W.setMobileGestures = function (z) {
    switch (z) {
      case 0:
        W.img.viewer.panVertical = false;
        W.img.viewer.panHorizontal = false;
        break;
      case 1:
        W.img.viewer.panVertical = false;
        W.img.viewer.panHorizontal = true;
        break;
      default:
        W.img.viewer.panVertical = true;
        W.img.viewer.panHorizontal = true;
    }
  };

  W.manageMobileDrag = function (e) {
    if (W.status.interaction.pinchZooming) {
      e.preventDefaultAction = true;
    } else {
      e.preventDefaultAction = false;
    }
  };

  W.manageMobileDragEnd = function (e) {
    if (W.status.interaction.pinchZooming) {
      e.preventDefaultAction = true;
    } else {
      e.preventDefaultAction = false;
    }
  };

  W.manageMobileTap = function (e) {
    console.log(`mobile tap ${e}`);
    W.anyUserAction();
    var z = W.img.getZoomLevel(true);
    var posY = W.img.getCenter().y;
    var posX = W.img.viewer.viewport.pointFromPixel(e.position).x;
    var targetHorizontalCenter = new OpenSeadragon.Point(posX, posY);
    var leftBound = W.img.viewer.viewport.getHomeBounds().x;
    var rightBound = leftBound + W.img.viewer.viewport.getHomeBounds().width;
    var eventY = W.img.viewer.viewport.pointFromPixel(e.position).y;
    var targetCenter = new OpenSeadragon.Point(posX, eventY);
    switch (z) {
      case 0:
        if (e.quick) {
          W.img.touchZoomToCenter(e);
          W.img.panTo(targetHorizontalCenter);
        }
        break;
      case 1:
        if (e.quick && posX > leftBound && posX < rightBound
          && !W.status.interaction.pinchZooming) {
          W.img.panTo(targetHorizontalCenter);
        }
        break;
      case 2:
        if (e.quick && posX > leftBound && posX < rightBound
         && !W.status.interaction.pinchZooming) {
          W.img.panTo(targetCenter);
          var si = W.randomInt(0, 9);
          W.actionSounds[si].fire();
        }
        break;
      case 3:
        if (e.quick && posX > leftBound && posX < rightBound
         && !W.status.interaction.pinchZooming) {
          W.img.panTo(targetCenter);
          var si = W.randomInt(0, 19);
          W.actionSounds[si].fire();
        }
        break;
      case 4:
        if (e.quick && posX > leftBound && posX < rightBound
         && !W.status.interaction.pinchZooming) {
          W.img.panTo(targetCenter);
          var si = W.randomInt(10, 19);
          W.actionSounds[si].fire();
        }
        break;
      case 5:
        if (e.quick && posX > leftBound && posX < rightBound
         && !W.status.interaction.pinchZooming) {
          W.img.panTo(targetCenter);
          var si = W.randomInt(20, 25);
          W.actionSounds[si].fire();
        }
        break;
    }
  };

  W.manageMobilePinch = function (e) {
    if (W.status.interaction.insideApp) {
      var z = W.img.getZoomLevel(true);
      var dir = e.distance - e.lastDistance;
      W.dLog && console.log(`gest dir ${dir}`);
      var thisPinchTime = Date.now();
      var deltaPinchTime = thisPinchTime -
        W.status.interaction.lastPinchTime;
      if (dir > 0) {
        W.dLog && console.log(`gest SPREAD ${e.distance}`);
      } else {
        W.dLog && console.log(`gest PINCH ${e.distance}`);
      }
      if (dir > 10 && e.distance > 230 && deltaPinchTime > W.status.interaction.pinchSleepTime) {
        W.status.interaction.lastPinchTime = thisPinchTime;
        W.status.interaction.pinchZooming = true;
        W.dLog && console.log('gest SPREAD ZOOOM IN!');
        if (z > 0 && z < 5) {
          W.img.doSingleZoomIn();
        }
      }
      if (dir < -10 && e.distance < 300 && deltaPinchTime > W.status.interaction.pinchSleepTime) {
        W.status.interaction.lastPinchTime = thisPinchTime;
        W.status.interaction.pinchZooming = true;
        W.dLog && console.log('gest PINCH ZOOOM OUT!');
        if (z !== 0) {
          W.img.doSingleZoomOut();
        }
      }
    }
  };

  /**
   * @function setMobileAudioEffects prepare audio effects
   * @param {Number} layer layer number
   **/
  W.setMobileAudioEffects = function (layer) {
    var effectOptions,
      effectOptionsUpper,
      effectOptionsLower,
      effectOptionsOff;
    switch (layer) {
      case 1:
        effectOptions = {
          lpFilter: false,
          reverb: false,
          stereoPanner: true
        };
        break;
      case 2:
        effectOptions = {
          lpFilter: false,
          reverb: false,
          stereoPanner: false
        };
        effectOptionsUpper = {
          lpFilter: false,
          reverb: false,
          stereoPanner: true
        };
        effectOptionsLower = {
          lpFilter: false,
          reverb: false,
          stereoPanner: true
        };
        break;
      case 3:
        effectOptions = {
          lpFilter: false,
          reverb: false,
          stereoPanner: true
        };
        break;
      case 4:
        effectOptions = {
          lpFilter: false,
          reverb: false,
          stereoPanner: false
        };
        effectOptionsUpper = {
          lpFilter: false,
          reverb: false,
          binaural: false
        };
        break;
      case 5:
        effectOptions = {
          lpFilter: false,
          reverb: false,
          stereoPanner: true
        };
        break;
    }
    effectOptionsOff = {
      lpFilter: false,
      distortion: false,
      hpFilter: false,
      reverb: false,
      binaural: false
    };
    // Effect initialization
    if (layer !== 0) {
      var atttractors = W.uniqueStreamArray(W.streamAttractors[layer]);
      // atttractors.forEach(function (el) {
      //     W.wallSounds[el.stream].setEffectChain(effectOptionsOff, W.reverberator);
      //     W.dLog && console.log(`Effect chain init for CURRENT LAYER stream ${el.stream}`);
      // });
      atttractors.forEach(function (el) {
        if (el.fromLayer === "upper") {
          W.wallSounds[el.stream].setEffectChain(effectOptionsUpper, W.reverberator);
          W.dLog && console.log(`Effect chain init for UPPER stream ${el.stream}`);
        } else if (el.fromLayer === "lower") {
          W.wallSounds[el.stream].setEffectChain(effectOptionsLower, W.reverberator);
          W.dLog && console.log(`Effect chain init for LOWER stream ${el.stream}`);
        } else {
          W.wallSounds[el.stream].setEffectChain(effectOptions, W.reverberator);
          W.dLog && console.log(`Effect chain init for CURRENT LAYER stream ${el.stream}`);
        }
      });
    }
  };

  W.manageMobilePan = function (e) {
    if (W.status.interaction.insideApp) {
      if (!W.status.interaction.panSleep) {
        var z = W.img.getZoomLevel(false);
        var center = W.img.getCenter();
        switch (z) {
          case 4:
            W.insideAttractors.forEach(function(el) {
              if (!el.omni) {
                var attractor = new OpenSeadragon.Point(el.x, el.y);
                var distance = center.distanceTo(attractor);
                var gain = 1 / Math.pow(1 + distance, 30);
                W.wallSounds[el.stream].fadeToGain(gain);
                W.dLog && console.log(`L4 - Stream: ${el.stream} | Dist: ${distance} | MasterGain: ${gain}`);
              }
            });
            break;
          case 5:
            // do nothing
            break;
          default:
            // For each attractor inside PA
            W.insideAttractors.forEach(function(el) {
              if (!el.omni) {
                // **** STEREO PANNER *****
                var c1 = center.x - el.x;
                var c2 = center.y - el.y;
                var r2 = Math.pow(c1, 2) + Math.pow(c2, 2);
                // Clip values to avoid abrupt passage left/right if close to attr. point
                var K2 = Math.pow(0.02, 2); // Clipping area const = 4*10^-4
                if (r2 < K2) {
                  c2 = W.sign(c2) * Math.sqrt(K2 - Math.pow(c1, 2));
                  W.dLog && console.log(`Clipping c2...`);
                }
                var newPos = c1 / (c2 * Math.sqrt((Math.pow(c1, 2) / Math.pow(c2, 2)) + 1))
                var oldPos = W.wallSounds[el.stream].stereoPanner.getPosition();
                W.wallSounds[el.stream].stereoPanner.interpolatePosition(oldPos, newPos);
                // **** DRY GAIN *****
                // Calc gain
                var attractor = new OpenSeadragon.Point(el.x, el.y);
                var distance = center.distanceTo(attractor);
                var gain = 1 / Math.pow(1 + distance, 50);
                // Assign to dry gain
                W.wallSounds[el.stream].fadeToGain(gain, 0.8);
                W.dLog && console.log(`L1 - Stream: ${el.stream} | Dist: ${distance} | Stereo pos: ${newPos} | Gain ${gain}`);
              }
            });
          break;
        }
        W.status.interaction.panCount++;
        W.status.interaction.panSleep = true;
        setTimeout(function () {
          W.status.interaction.panSleep = false;
        }, 850);
        W.dLog && console.log(`Pan count: ${W.status.interaction.panCount}`);
        if (W.status.interaction.panCount > 4) {
          W.status.interaction.panCount = 0;
          W.img.viewer.raiseEvent('animation-finish');
          W.dLog && console.log(`Pan events limit reached. Raising animation finish event`);
        }
      }
    }
  };


  // ***************************/
  // ***************************/
  // ******** UTILITIES ********
  /**
   * @function uniqueStreamArray Filters duplicates by stream number
   * @param {Array} attrArray array of stream attractors
   * @returns Array with only one stream attractor for each stream
   **/
  W.uniqueStreamArray = function (attrArray) {
    // Filter out duplicates by same stream
    return attrArray.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj.stream).indexOf(obj.stream) === pos;
    });
  };

  /**
   * @function streamDiff Performs difference on attractors arrays based on stream ID
   * @param {Array} attrArray1 array of stream attractors
   * @param {Array} attrArray2 array of stream attractors
   * @returns Array with the attractors of attrArray1 minus the attractors of attrArray2 (based on streamID)
   **/
  W.streamDiff = function (attrArray1, attrArray2) {
    return attrArray1.filter((i) => {
      return attrArray2.map(mapObj => mapObj.stream).indexOf(i.stream) === -1;
    });
  };

  /**
   * Removes an attractor from an attractor array, based on stream ID
   * @param {Array} attrArray array of stream attractors
   * @param {Object} streamAttr stream attractor to be removed
   * @returns Array with the original attracotrs minus the input attractor (based on streamID)
   **/
  W.removeStreamAttractor = function (attrArray, streamAttr) {
    attrArray.some(function(item, index) {
      if(attrArray[index].stream === streamAttr.stream) {
        attrArray.splice(index, 1);
        return true;
      }
      return false;
    });
    return attrArray;
  };

  /**
   * Sorts by distance and filters duplicates by stream number
   * @param {Array} myArr array of objects {distance, attractor}
   * @returns Array with only the closest stream attractor for each stream
   **/
  W.uniqueClosestStreamsArray = function (myArr) {
    // Sort by distance
    myArr = myArr.sort((a, b) => a.dist - b.dist);
    // Get only attractor objects
    myArr = myArr.map(el => el.attr);
    // Filter out duplicates by same stream
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj.stream).indexOf(obj.stream) === pos;
    });
  };

  /**
   * Converts from degrees to radians
   * @param {Number} degrees
   * @returns {Number} radians
   **/
  W.radians = function (degrees) {
    return degrees * Math.PI / 180;
  };

  /**
   * Converts from radians to degrees
   * @param {Number} radians
   * @returns {Number} degrees
   **/
  W.degrees = function (radians) {
    return radians * 180 / Math.PI;
  };

  /**
   * @param {Number} val
   * @returns {Number} 1 when positive or null, -1 when negative
   **/
  W.sign = function (val) {
    return val >= 0 ? 1 : -1;
  };

  /**
   * Maps from a range to another
   * @param {Number} value value to be mapped
   * @param {Number} istart original range lower bound
   * @param {Number} istop original range upper bound
   * @param {Number} ostart destination range lower bound
   * @param {Number} ostop destination range upper bound
   * @returns {Number} mapped value
   **/
  W.map = function (value, istart, istop, ostart, ostop) {
    return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
  };

  /**
   * @param {Number} min
   * @param {Number} max
   * @returns {Number} Random integer number. Max and min both inclusive
   **/
  W.randomInt = function (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  /**
   * @param {Number} min
   * @param {Number} max
   * @returns {Number} Random float number with three decimals. Min inclusive max exclusive
   **/
  W.randomFloat = function (min, max) {
    var n = Math.random() * (max - min) + min;
    return Number.parseFloat(n.toFixed(3));
  };

 /**
  * @function expSmooth exponential smoothing
  * @param {Number} curVal Value to be smoothed
  * @param {Number} prevVal Previous value got from the source
  * @param {Number} N Equivalent moving average window length
  * @returns {Number} interpolated value
  * Usage example: sound.setGain(expSmooth(newGain, sound.getMasterGain(), 5));
  **/
  W.expSmooth = function (currVal, prevVal, N) {
    var tiny = 1 - (1/N);
    return tiny * prevVal + (1 - tiny) * currVal;
  };

  /**
   * @function isEven true if x is even
   * @param {Number} x integer number
   **/
  W.isEven = function (x) {
    return (x%2) === 0;
  };

  /**
   * @function getJSON json AJAX call
   * @param {String} url url of json file
   * @param {Function} callback to be called whan json is retrieved
   **/
  W.getJSON = function (url, callback) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.responseType = 'json';
      xhr.onload = function() {
        var status = xhr.status;
        if (status === 200) {
          callback(null, xhr.response);
        } else {
          callback(status, xhr.response);
        }
      };
      xhr.send();
  };

  /**
   * @function raiseLandingEvent raises DOM custom events at new layer landing
   * @param {Number} l layer Number
   **/
  W.raiseLandingEvent = function (l, firstLand) {
    let el = document;
    let landingEvent = new CustomEvent('layer-landing', {detail: l});
    el.dispatchEvent(landingEvent);
    if (firstLand) {
      let onceLandingEvent = new CustomEvent('once-layer-landing', {detail: l});
      el.dispatchEvent(onceLandingEvent);
    }
  };

  /**
   * @function checkPlayingAudios
   * @returns {Array} array of current playing wall sounds
   **/
  W.checkPlayingAudios = function () {
    var activeAudiosIndex = [];
    for(var i = 0; i < W.wallSounds.length; i++) {
      if (W.wallSounds[i].isPlaying()) {
        activeAudiosIndex.push(i);
      }
    }
    return activeAudiosIndex;
  };

  /**
   * @function muteAll
   **/
  W.muteAll = function () {
    W.wallSounds.forEach(function (el) {
      el.mute();
    });
    W.actionSounds.forEach(function (el) {
      el.mute();
    });
    W.status.audio.appMuted = true;
  };

  /**
   * @function unMuteAll
   **/
  W.unMuteAll = function () {
    W.wallSounds.forEach(function (el) {
      el.unmute();
    });
    W.actionSounds.forEach(function (el) {
      el.unmute();
    });
    W.status.audio.appMuted = false;
  };


})(WebWallWhispers);

window.addEventListener('load', WebWallWhispers.onLoad, false);
