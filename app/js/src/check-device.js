// Web Wall Whispers
//
// Interactive web-based audio-visual installation.
// Developed for Fondazione Spinola-Banna per l'Arte.
// Copyright (C) 2018 Francesco Cretti
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see
// https://bitbucket.org/splsteam/splswebapp/src/master/LICENSE.txt.
//
// JS developer - francesco.cretti@gmail.com
// Coordinator - leehooni@gmail.com
//
// Home Page: www.webwallwhispers.net
// Bug report: https://bitbucket.org/splsteam/splswebapp/issues

var WebWallWhispers = WebWallWhispers === undefined ? {} : WebWallWhispers;

WebWallWhispers.checkDevice = {};

function gcd (a, b) {
  return (b == 0) ? a : gcd (b, a%b);
}

WebWallWhispers.checkDevice.isMobile = {
  Android: function() {
    var m = navigator.userAgent.match(/Android/i);
    if (m === null) {
      return false;
    } else {
      return true;
    }
  },
  BlackBerry: function() {
    var m = navigator.userAgent.match(/BlackBerry/i);
    if (m === null) {
      return false;
    } else {
      return true;
    }
  },
  iOS: function() {
    var m = navigator.userAgent.match(/iPhone|iPad|iPod/i);
    if (m === null) {
      return false;
    } else {
      return true;
    }
  },
  Opera: function() {
    var m = navigator.userAgent.match(/Opera Mini/i);
    if (m === null) {
      return false;
    } else {
      return true;
    }
  },
  Windows: function() {
    var m = navigator.userAgent.match(/IEMobile/i);
    if (m === null) {
      return false;
    } else {
      return true;
    }
  },
  any: function() {
    return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
  }
};

WebWallWhispers.checkDevice.getAspectRatio = function () {
  var w = screen.width;
  var h = screen.height;
  var r = gcd(w, h);
  var arString = `${w/r}:${h/r}`;
  return arString;
};

WebWallWhispers.checkDevice.getScreenSize = function () {
  var sString = `${screen.width}x${screen.height}`;
  return {
    string: sString,
    width: screen.width,
    height: screen.height,
    area: this.getScreenArea()
  };
};

WebWallWhispers.checkDevice.getScreenArea = function () {
  return screen.width * screen.height;
};

WebWallWhispers.checkDevice.isIE = function () {
  // Internet Explorer 6-11
  // return /*@cc_on!@*/false || !!document.documentMode;
  return bowser.msie ? true : false;
};

WebWallWhispers.checkDevice.isEdge = function () {
  // Edge 20+
  // return !this.isIE() && !!window.StyleMedia;
  return bowser.msedge ? true : false;
};

WebWallWhispers.checkDevice.isSafari = function () {
  // Safari 3.0+ "[object HTMLElementConstructor]"
  // return /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
  return bowser.safari ? true : false;
};

WebWallWhispers.checkDevice.isChrome = function () {
  // Chrome 1+
  // return !!window.chrome && !!window.chrome.webstore;
  return bowser.chrome ? true : false;
};

WebWallWhispers.checkDevice.isOpera = function () {
  // Opera 8.0+
  // return (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  return bowser.opera ? true : false;
};

WebWallWhispers.checkDevice.isFirefox = function () {
  // Firefox 1.0+
  // return typeof InstallTrigger !== 'undefined';
  return bowser.firefox ? true : false;
};

WebWallWhispers.checkDevice.getBrowserSpecs = function () {
  return {
    name: bowser.name,
    version: bowser.version
  };
};

WebWallWhispers.checkDevice.isAllowed = function () {
  if (bowser.chrome && bowser.version >= 62) {
    return true;
  } else if (bowser.chromium && bowser.version >= 62) {
    return true;
  } else if (bowser.firefox && bowser.version >= 57) {
    return true;
  } else if (bowser.safari && bowser.version >= 10) {
    return true;
  } else if (bowser.msedge && bowser.version >= 16) {
    return true;
  } else if (bowser.opera && bowser.version >= 49) {
    return true;
  } else if (bowser.android && bowser.chrome && bowser.version >= 62) {
    return true;
  } else if (bowser.ios && bowser.version >= 11) {
    return true;
  } else {
    return false;
  }
};

WebWallWhispers.checkDevice.liteVersion = function () {
  if (bowser.safari || bowser.ios || bowser.msedge || bowser.firefox) {
    return true;
  } else {
    return false;
  }
};
