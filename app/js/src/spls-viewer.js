// Web Wall Whispers
//
// Interactive web-based audio-visual installation.
// Developed for Fondazione Spinola-Banna per l'Arte.
// Copyright (C) 2018 Francesco Cretti
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see
// https://bitbucket.org/splsteam/splswebapp/src/master/LICENSE.txt.
//
// JS developer - francesco.cretti@gmail.com
// Coordinator - leehooni@gmail.com
//
// Home Page: www.webwallwhispers.net
// Bug report: https://bitbucket.org/splsteam/splswebapp/issues

/** ACTIVE ZONE PROTOTYPE
 * @param {Object} HTMLdoc DOM document object
 * @param {String} _id Unique string identifier of active zone
 * @param {Object} loc OpenSeaDragon Rec Object for define active zone location
 **/
function ActiveZone(HTMLdoc, _id, loc) {
  this.element = HTMLdoc.createElement('div');
  this.id = _id;
  this.location = loc;
  this.visible = false;
}

/**
 * @param {Array} rgbColor Array of RGB values
 * @param {Number} alpha Transparency value
 * @param {Number} z CSS z-index
 **/
ActiveZone.prototype.setStyle = function (rgbColor, alpha, z) {
  this.element.style.background = 'rgb('+rgbColor[0]+','+rgbColor[1]+','+rgbColor[2]+')';
  this.element.style.opacity = alpha.toString();
  this.element.style.zIndex = z.toString();
};

/**
 * @param {Function} callback Function to be called when clicked on active zone
 **/
ActiveZone.prototype.setAction = function (callback) {
  this.action = callback;
};

/**
 * @param {Number} val Starting opacity value for fade out
 * @param {Function} callback Function to be called at the end of fade
 **/
ActiveZone.prototype.opacityFadeOut = function (val, callback) {
  const self = this;
  if (isNaN(val)) {
    val = 0.9;
  }
  (function fade() {
    self.element.style.opacity = val;
    if (val > 0) {
      val -= 0.05;
      setTimeout(fade, 5);
    } else {
      if (callback) callback();
    }
  }());
};

/**
 * @param {Number} val Starting opacity value for fade in
 * @param {Number} fVal Ending opacity value for fade in
 * @param {Function} callback Function to be called at the end of fade
 **/
ActiveZone.prototype.opacityFadeIn = function (val, fVal, callback) {
  const self = this;
  if (isNaN(val)) {
    val = 0;
  }
  (function fade() {
    self.element.style.opacity = val;
    if (val < fVal) {
      val += 0.02;
      setTimeout(fade, 40);
    } else {
      if (callback) callback();
    }
  }());
};

/** MAIN IMAGE VIEWER (IMPLEMENTED WITH OpenSeaDragon)
 * @function SplsViewer constructor
 * @param {Object} HTMLdoc DOM document object
 * @param {Object} imageConfig
 * @param {Boolean} mobile
 * @param {Boolean} iOS
 **/
function SplsViewer(HTMLdoc, imageConfig, mobile, iOS) {
  this.document = HTMLdoc;
  this.mobile = mobile;
  this.debugMode = imageConfig.debugMode;
  this.zooms = imageConfig.zooms;
  this.acZones = [];
  this.viewer = OpenSeadragon(imageConfig.osdinit);
  this.homeBox = new OpenSeadragon.Rect(imageConfig.homeBox.x,
    imageConfig.homeBox.y, imageConfig.homeBox.width, imageConfig.homeBox.height);
  this.clipBox = new OpenSeadragon.Rect(imageConfig.clipBox.x,
    imageConfig.clipBox.y, imageConfig.clipBox.width, imageConfig.clipBox.height);
  this.viewer.debugMode = this.debugMode;
  this.viewer.iOSDevice = iOS;
  this.debugMode && console.log(`iOS set to ${iOS}`);
  if (this.mobile) {
    this.viewer.immediateRender = true;
    this.viewer.animationTime = 1;
    this.viewer.springStiffness = 6.5;
  } else {
    this.viewer.immediateRender = false;
  }
  this.colorManager = new Colors();
  this.autoPilot = {
    actionsId: [],
    active: false,
    step: 0
  };
  this.fullyLoaded = false;
  this.addAutopilotFunctions();
}

/**
 * @param {Number} _l Level number to be mapped
 * @returns {Number} level mapped value
 **/
SplsViewer.prototype.mapLevel = function (_l, maxZoom) {
  var l = _l/maxZoom;
  this.debugMode && console.log(`not mapped ${_l} | mapped level to image: ${l.toFixed(5)}`);
  return l;
};

/**
 * @function setZoomLevels
 * @param {Array} _z Array of zooms level that will be allowed
 * @param {Number} maxZoom Max possible zoom for the current view
 **/
SplsViewer.prototype.setZoomLevels = function (_z, maxZoom) {
  var z = [];
  for (let i = 0; i < _z.length; i++) {
    z[i] = this.mapLevel(_z[i], maxZoom);
  }
  this.viewer.zoomLevels ({
    levels: z,
  });
};

/**
 * @function init
 * @param {Object} configMaxZoom
 **/
SplsViewer.prototype.init = function (configMaxZoom, fullyLoadedCallback) {
  const self = this;
  if (self.mobile) {
    self.viewer.navigator.element.style.display = 'none';
    this.debugMode && console.log(`navigator hidden ${self.mobile}`);
  }
  // Clip view
  self.viewer.addHandler('open', (function (v) {
    return function () {
      var maxZoom = v.viewport.getMaxZoom();
      console.log(`Max possible zoom ${maxZoom}`);
      self.setZoomLevels(self.zooms, maxZoom);
      v.viewport.maxZoomLevel = configMaxZoom;
      v.viewport.fitBounds(self.homeBox, true);
      var tiledImage = v.world.getItemAt(0);
      tiledImage.setClip(self.clipBox);
      tiledImage.addOnceHandler('fully-loaded-change', fullyLoadedCallback);
    };
  })(self.viewer));

  // If not fully loaded in 10 seconds try to reset
  self.resetTimeout = setTimeout(function checkReset() {
    var tiledImage = self.viewer.world.getItemAt(0);
    if (!tiledImage.getFullyLoaded()) {
      console.log('Resetting view');
      tiledImage.reset();
      self.resetTimeout = setTimeout(checkReset, 10000);
    }
  }, 10000);

  if (self.debugMode) {
    self.viewer.addHandler('open', (function (v) {
      return function () {
        var coordEl = self.document.getElementById('coordinates');
        var tracker = new OpenSeadragon.MouseTracker({
          element: v.container,
          moveHandler: function (event) {
            var webPoint = event.position;
            var viewportPoint = v.viewport.pointFromPixel(webPoint);
            var imagePoint = v.viewport.viewportToImageCoordinates(viewportPoint);
            coordEl.innerHTML = 'MOUSE POINTER COORDINATES<br><br>Web:<br>' + webPoint.toString() +
            '<br><br>Viewport:<br>' + viewportPoint.toString() +
            '<br><br>Image:<br>' + imagePoint.toString();
          }
        });
        tracker.setTracking(true);
       };
    })(self.viewer));
  }
  self.setActiveZones();
};



/**
 *  * @function matchZoomLevel
 * @param {Number} _l Level number to match with allowed zooms
 * @returns {Number} Allowed level value (viewport coords) closest to input value
 **/
SplsViewer.prototype.matchZoomLevel = function (_l) {
  var distance = Math.abs(this.zooms[0]-_l);
  var idx = 0;
  for (var i=1; i<this.zooms.length; i++) {
    var cdistance = Math.abs(this.zooms[i]-_l);
    if (cdistance < distance) {
      idx = i;
      distance = cdistance;
    }
  }
  return this.zooms[idx];
};

/**
 * @function getMatchedZoomLevelIndex
 * @param {Number} _l Level number to match with allowed zooms
 * @returns {Number} Index of allowed zoom level closest to input
 **/
SplsViewer.prototype.getMatchedZoomLevelIndex = function (_l) {
  var distance = Math.abs(this.zooms[0]-_l);
  var idx = 0;
  for (var i=1; i<this.zooms.length; i++) {
    var cdistance = Math.abs(this.zooms[i]-_l);
    if (cdistance < distance) {
      idx = i;
      distance = cdistance;
    }
  }
  return idx;
};

/**
 * @function goHome
 **/
SplsViewer.prototype.goHome = function () {
  this.viewer.viewport.goHome();
};

/**
 * @function doSingleZoomIn
 **/
SplsViewer.prototype.doSingleZoomIn = function () {
  this.viewer.viewport.zoomBy(
    this.viewer.zoomPerClick / 1.0
  );
};

/**
 * @function doSingleZoomOut
 **/
SplsViewer.prototype.doSingleZoomOut = function () {
  if (this.getZoomLevel() === 1) {
    this.goHome();
  } else {
    this.viewer.viewport.zoomBy(
      1.0 / this.viewer.zoomPerClick
    );
  }
};

/**
 * @function alignVertically
 **/
SplsViewer.prototype.alignVertically = function () {
  var x = this.viewer.viewport.getCenter().x;
  var hy = this.viewer.viewport.getHomeBounds().y;
  var h = this.viewer.viewport.getHomeBounds().height;
  var y = (2 * hy + h) / 2;
  this.debugMode && console.log(`Align vertically x ${x}, hy ${hy}, h ${h}, y ${y}`);
  var target = new OpenSeadragon.Point(x, y);
  this.viewer.viewport.panTo(target);
};

/**
 * @function alignHorizontally
 **/
SplsViewer.prototype.alignHorizontally = function () {
  var y = this.viewer.viewport.getCenter().y;
  var wx = this.viewer.viewport.getHomeBounds().x;
  var w = this.viewer.viewport.getHomeBounds().width;
  var x = (2 * wx + w) / 2;
  this.debugMode && console.log(`Align horizontally x ${x}, wx ${wx}, w ${w}, x ${x}`);
  var target = new OpenSeadragon.Point(x, y);
  this.viewer.viewport.panTo(target);
};

/**
 * @function scrollZoomToPointer
 * @param {Object} event
 **/
SplsViewer.prototype.scrollZoomToPointer = function (event) {
  if (!event.preventDefaultAction && this.viewer.viewport) {
    factor = Math.pow( this.viewer.zoomPerScroll, event.scroll);
    this.viewer.viewport.zoomBy(
      factor,
      this.viewer.viewport.pointFromPixel(event.position, true)
    );
  }
};

/**
 * @function scrollZoomToCenter
 * @param {Object} event
 **/
SplsViewer.prototype.scrollZoomToCenter = function (event) {
  if (!event.preventDefaultAction && this.viewer.viewport) {
    factor = Math.pow(this.viewer.zoomPerScroll, event.scroll);
    this.viewer.viewport.zoomBy(factor);
  }
};

/**
 * @function touchZoomToPointer
 * @param {Object} event
 **/
SplsViewer.prototype.touchZoomToPointer = function (event) {
  if (this.viewer.viewport && event.quick) {
    this.viewer.viewport.zoomBy(
      event.shift ? 1.0 / this.viewer.zoomPerClick : this.viewer.zoomPerClick,
      this.viewer.viewport.pointFromPixel(event.position, true)
    );
  }
};

/**
 * @function touchZoomToCenter
 * @param {Object} event
 **/
SplsViewer.prototype.touchZoomToCenter = function (event) {
  if (this.viewer.viewport && event.quick) {
    this.viewer.viewport.zoomBy(this.viewer.zoomPerClick);
    this.viewer.viewport.applyConstraints();
  }
};

/**
 * @function on
 * @param {String} event Event to listen to
 * @param {Function} callback Callback function
 **/
SplsViewer.prototype.on = function (event, callback) {
  'use strict';
  const self = this;
  self.viewer.addHandler(event, function (e) {
    callback(e);
  });
};

/**
 * @function once
 * @param {String} event Event to listen to
 * @param {Function} callback Callback function
 **/
SplsViewer.prototype.once = function (event, callback) {
  'use strict';
  const self = this;
  self.viewer.addOnceHandler(event, function (e) {
    callback(e);
  });
};

/**
 * @function getZoomValue
 * @param {Boolean} current true for the current location; defaults to false (target location)
 * @returns {Number} location zoom level value in viewport coords [2.55-360]
 **/
SplsViewer.prototype.getZoomValue = function (current) {
  return this.viewer.viewport.getZoom(current);
};

/**
 * @function getZoomLevel
 * @param {Boolean} current true for the current location; defaults to false (target location)
 * @returns {Number} location zoom level index (0-5)
 **/
SplsViewer.prototype.getZoomLevel = function (current) {
  var z = this.viewer.viewport.getZoom(current);
  return this.getMatchedZoomLevelIndex(z);
};

/**
 * @function getCenter
 * @param {Boolean} current true for the current location; defaults to false (target location)
 * @returns {Object} OpenSeaDragon.Point, the center point
 **/
SplsViewer.prototype.getCenter = function (current) {
  return this.viewer.viewport.getCenter(current);
};

/**
 * @function zoomTo
 * @param {Number} zoom The zoom level to zoom to
 * @param {Object} refPoint OSD Point object: point which will stay at the same
 *                          screen location. Defaults to the viewport center
 * @param {Boolean} immediately If true skip amiation. Defaults false
 **/
SplsViewer.prototype.zoomTo = function (zoom, refPoint, immediately) {
  var self = this;
  var z = self.matchZoomLevel(zoom);
  self.viewer.viewport.zoomTo(z, refPoint, immediately);
};

/**
 * @function panTo
 * @param {Object} center OSD Point object: center of final view
 * @param {Boolean} immediately If true skip amiation. Defaults false
 **/
SplsViewer.prototype.panTo = function (center, immediately) {
  this.viewer.viewport.panTo(center, immediately);
};

/**
 * @function setActiveZones
 **/
SplsViewer.prototype.setActiveZones = function () {
  const self = this;
  // CHILD ACTIVE ZONE
  var childLoc = new OpenSeadragon.Rect(0.246, 0.47, 0.005, 0.01);
  var child = new ActiveZone(self.document, 'child', childLoc);
  child.setStyle([255, 0, 0], 0.6, 3);
  child.setAction(function () {
    var targetsx = new OpenSeadragon.Point(0.05, 0.47);
    self.withSlowAnimation(function () {
      self.viewer.viewport.zoomTo(self.zooms[2]);
      self.viewer.viewport.panTo(targetsx);
    });
  });
  self.acZones[0] = child;
};

/**
 * @function hideActiveZone
 * @param {String} id Unique Id of active zone
 **/
SplsViewer.prototype.showActiveZone = function (id) {
  const self = this;
  var ac = self.getActiveZoneById(id);
  ac.opacityFadeIn(0, 0.6);
  self.viewer.addOverlay({
    element: ac.element,
    location: ac.location,
  });
  ac.element.addEventListener('click', ac.action);
  ac.visible = true;
};

/**
 * @function hideActiveZone
 * @param {String} id Unique Id of active zone
 **/
SplsViewer.prototype.hideActiveZone = function (id) {
  const self = this;
  var ac = this.getActiveZoneById(id);
  ac.opacityFadeOut(ac.element.style.opacity, function () {
    self.viewer.removeOverlay(ac.element);
    ac.element.removeEventListener('click', ac.action, true);
    ac.visible = false;
  });
};

/**
 * @function getActiveZoneById
 * @param {String} id Unique Id of active zone
 * @returns {ActiveZone}
 **/
SplsViewer.prototype.getActiveZoneById = function (id) {
  for (var i = 0; i < this.acZones.length; i++) {
    if (id.localeCompare(this.acZones[i].id) === 0) {
      return this.acZones[i];
    }
  }
};

/**
 * @function getColorsWeight
 * @returns {Array} returns an array containing [r,g,b,a] values from the view
 **/
SplsViewer.prototype.getColorsWeight = function () {
  var canvasScaleFactor = OpenSeadragon.pixelDensityRatio;
  var ctx = this.viewer.drawer.context;
  var v = this.viewer.viewport;

  var N = 1;
  // Get image data of 1/N of the view area
  var canvasBounds = v.viewportToViewerElementRectangle(v.getBounds());
  var posX = (canvasBounds.width - canvasBounds.width / Math.sqrt(N)) / 2;
  var posY = (canvasBounds.height - canvasBounds.height / Math.sqrt(N)) / 2;
  var x = posX * canvasScaleFactor;
  var y = posY * canvasScaleFactor;
  var width = canvasBounds.width / Math.sqrt(N) * canvasScaleFactor;
  var height = canvasBounds.height / Math.sqrt(N) * canvasScaleFactor;
  // Color data
  var colorData = ctx.getImageData(x, y, width, height).data;
  // Decimation factor
  var M = 100;
  // Colors accumulators
  var maroon = 0;
  var green = 0;
  var grey = 0;
  var yellow = 0;
  var tI = performance.now();
  for (var i = 0; i < colorData.length; i += 4*M)
  {
    // Read color
    var c = new RGB(colorData[i], colorData[i+1], colorData[i+2]);
    // Find match
    var m = this.colorManager.matchColor(c);
    //var m = 0;
    // Increment accumulators
    switch (m) {
      case 0:
        maroon++;
        break;
      case 1:
        green++;
        break;
      case 2:
        grey++;
        break;
      case 3:
        yellow++;
        break;
    }
  }
  var pixelN = Math.floor(colorData.length / (4*M));

  var tF = performance.now();
  //this.debugMode && console.log(`Pixels analyzed: ${colorData.length}/(4*${M}) = ${pixelN}`);
  this.debugMode && console.log(`Pixel analysis took ${tF - tI} millisecs`);

  if (this.debugMode) {
    // Draw color picker target area
    ctx.beginPath();
    ctx.strokeStyle = "#800080";
    ctx.lineWidth = 10;
    ctx.rect(x, y, width, height);
    ctx.stroke();
  }
  //return this.colorManager.matchColor(centerColor);
  var maroonGain = Number.parseFloat((maroon/pixelN).toFixed(3));
  var greenGain = Number.parseFloat((green/pixelN).toFixed(3));
  var greyGain = Number.parseFloat((grey/pixelN).toFixed(3));
  var yellowGain = Number.parseFloat((yellow/pixelN).toFixed(3));
  return [maroonGain, greenGain, greyGain, yellowGain];
};

/**
 * @function drawPlayArea
 * @param {Number} ratio
 * @param {Array} streamAttractors
 **/
SplsViewer.prototype.drawPlayArea = function (ratio, streamAttractors) {
  if (this.debugMode && !this.mobile) {
    var canvasScaleFactor = OpenSeadragon.pixelDensityRatio;
    var ctx = this.viewer.drawer.context;
    var v = this.viewer.viewport;
    // Center
    var bounds = v.getBounds();
    var canvasBounds = v.viewportToViewerElementRectangle(bounds);
    var x = canvasBounds.getCenter().x * canvasScaleFactor;
    var y = canvasBounds.getCenter().y * canvasScaleFactor;
    // Ratio
    var viewportCenter = bounds.getCenter();
    var ratioPoint = new OpenSeadragon.Point(viewportCenter.x, viewportCenter.y + ratio);
    var canvasRatioPoint = v.pixelFromPoint(ratioPoint);
    var canvasRatio = Math.abs(canvasRatioPoint.y - canvasBounds.getCenter().y);
    // Draw style
    ctx.lineWidth = 8;
    ctx.strokeStyle = "#00b300";
    //Draw circle
    ctx.beginPath();
    ctx.arc(x, y, canvasRatio * canvasScaleFactor, 0, 2 * Math.PI);
    ctx.stroke();
    // Draw rectangle
    ctx.beginPath();
    ctx.strokeStyle = "#0033cc";
    ctx.rect(canvasBounds.x, canvasBounds.y, canvasBounds.width * canvasScaleFactor,
      canvasBounds.height * canvasScaleFactor);
    ctx.stroke();
    // Draw stream attractors
    ctx.lineWidth = 10;
    ctx.strokeStyle = '#f442f1';
    for (let i = 0; i < streamAttractors.length; i++) {
      //this.debugMode && console.log('Drawing stream attractor');
      var attr = new OpenSeadragon.Point(streamAttractors[i].x,
        streamAttractors[i].y);
      var canvasAttr = v.viewportToViewerElementCoordinates(attr);
      ctx.beginPath();

      ctx.arc(canvasAttr.x * canvasScaleFactor, canvasAttr.y * canvasScaleFactor,
         20, 0, 2 * Math.PI);
      ctx.fillStyle = '#f442f1';
      ctx.fill();
      ctx.stroke();
    }
  }
};

/**
 * @function withSlowAnimation
 * @param {Function} action Action to be performed with slower animation
 * @param {Number} time
 * @param {Object} args
 **/
SplsViewer.prototype.withSlowAnimation = function (action, time, args) {
  if (time < 3 || time > 10 || time === undefined) {
    time = 4; // Default value
  }
  // save old one
  var oldValues = {};
  var v = this.viewer.viewport;
  oldValues.centerSpringXAnimationTime = v.centerSpringX.animationTime;
  oldValues.centerSpringYAnimationTime = v.centerSpringY.animationTime;
  oldValues.zoomSpringAnimationTime = v.zoomSpring.animationTime;
  // set slower times
  v.centerSpringX.animationTime = v.centerSpringY.animationTime = time;
  v.zoomSpring.animationTime = time;
  // perform action
  action.apply(v, args);
  // restore old values
  v.centerSpringX.animationTime = oldValues.centerSpringXAnimationTime;
  v.centerSpringY.animationTime = oldValues.centerSpringYAnimationTime;
  v.zoomSpring.animationTime = oldValues.zoomSpringAnimationTime;
};

/**
 * @function withFastAnimation
 * @param {Function} action Action to be performed with faster animation
 **/
SplsViewer.prototype.withFastAnimation = function (action, args) {
  // save old one
  var oldValues = {};
  var v = this.viewer.viewport;
  oldValues.centerSpringXAnimationTime = v.centerSpringX.animationTime;
  oldValues.centerSpringYAnimationTime = v.centerSpringY.animationTime;
  oldValues.zoomSpringAnimationTime = v.zoomSpring.animationTime;
  // set faster times
  v.centerSpringX.animationTime = v.centerSpringY.animationTime = 0.5;
  v.zoomSpring.animationTime = 0.5;
  // perform action
  action.apply(v, args);
  // restore old values
  v.centerSpringX.animationTime = oldValues.centerSpringXAnimationTime;
  v.centerSpringY.animationTime = oldValues.centerSpringYAnimationTime;
  v.zoomSpring.animationTime = oldValues.zoomSpringAnimationTime;
};

SplsViewer.prototype.addAutopilotFunctions = function () {
  const self = this;
  self.autoPilot.t = [10, 10, 10, 10, 18, 12, 12, 14, 12, 12, 12, 12, 12, 12,
    12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 14, 18];
  self.autoPilot.a = [
    function () {
      if (self.getZoomLevel() !== 0) {
        self.goHome();
      }
    }, // 0
    function () {
      self.withSlowAnimation(self.doSingleZoomIn);
    }, // 1 - go to layer 1
    function () {
      self.panTo(new OpenSeadragon.Point(0.198, 0.37084), false);
    }, // 2
    function () {
      self.panTo(new OpenSeadragon.Point(0.051, 0.37084), false);
    }, // 3
    function () {
      self.withSlowAnimation(self.panTo, 10, [new OpenSeadragon.Point(0.198, 0.37084), false]);
      setTimeout(function (){
        self.viewer.raiseEvent('animation-finish');
      }, 5000);
    }, // 4
    function () {
      self.withSlowAnimation(self.doSingleZoomIn);
      self.withSlowAnimation(self.panTo, 4, [new OpenSeadragon.Point(0.204, 0.366), false]);
    }, // 5 - go to layer 2
    function () {
      self.panTo(new OpenSeadragon.Point(0.151, 0.364), false);
    }, // 6
    function () {
      self.withSlowAnimation(self.panTo, 6, [new OpenSeadragon.Point(0.061, 0.378), false]);
    }, // 7
    function () {
      self.withSlowAnimation(self.doSingleZoomIn);
    }, // 8 - go to layer 3
    function () {
      self.withSlowAnimation(self.panTo, 6, [new OpenSeadragon.Point(0.086, 0.386), false]);
    }, // 9
    function () {
      self.withSlowAnimation(self.panTo, 4, [new OpenSeadragon.Point(0.113, 0.378), false]);
    }, // 10
    function () {
      self.withSlowAnimation(self.panTo, 4, [new OpenSeadragon.Point(0.160, 0.383), false]);
    }, // 11
    function () {
      self.panTo(new OpenSeadragon.Point(0.152, 0.376), false);
    }, // 12
    function () {
      self.panTo(new OpenSeadragon.Point(0.016, 0.374), false);
    }, // 13
    function () {
      self.withSlowAnimation(self.panTo, 4, [new OpenSeadragon.Point(0.019, 0.382), false]);
    }, // 14
    function () {
      self.panTo(new OpenSeadragon.Point(0.022, 0.363), false);
    }, // 15
    function () {
      self.withSlowAnimation(self.doSingleZoomIn);
    }, // 16 - go to layer 4
    function () {
      self.panTo(new OpenSeadragon.Point(0.086, 0.372), false);
    }, // 17
    function () {
      self.panTo(new OpenSeadragon.Point(0.139, 0.380), false);
    }, // 18
    function () {
      self.panTo(new OpenSeadragon.Point(0.181, 0.379), false);
    }, // 19
    function () {
      self.panTo(new OpenSeadragon.Point(0.220, 0.379), false);
    }, // 20
    function () {
      self.withSlowAnimation(self.panTo, 4, [new OpenSeadragon.Point(0.118, 0.379), false]);
    }, // 21
    function () {
      self.withSlowAnimation(self.doSingleZoomIn);
    }, // 22 - go to layer 5
    function () {
      self.withSlowAnimation(self.panTo, 2, [new OpenSeadragon.Point(0.124, 0.380), false]);
    }, // 23
    function () {
      self.withSlowAnimation(self.panTo, 2, [new OpenSeadragon.Point(0.124, 0.383), false]);
    }, // 24
    function () {
      self.withSlowAnimation(self.panTo, 4, [new OpenSeadragon.Point(0.161, 0.378), false]);
    }, // 25
    function () {
      self.withSlowAnimation(self.panTo, 3, [new OpenSeadragon.Point(0.172, 0.373), false]);
    } // 26
  ];
};

/**
 * @function autoPilotStart
 **/
SplsViewer.prototype.autoPilotStart = function () {
  const self = this;
  if (!self.autoPilot.active) {
    var i = 0;
    (function exec() {
      // Start over
      if (i >= self.autoPilot.a.length) {
        i = 0;
      }
      // Exec current action
      self.autoPilot.a[i]();
      // Schedule next
      self.autoPilot.actionsTimeout = setTimeout(exec, self.autoPilot.t[i] * 1000, ++i);
    }());
    document.dispatchEvent(new CustomEvent('autopilot-start'));
    self.autoPilot.active = true;
  }
};

/**
 * @function autoPilotExit
 **/
SplsViewer.prototype.autoPilotExit = function () {
  const self = this;
  clearTimeout(self.autoPilot.actionsTimeout);
  document.dispatchEvent(new CustomEvent('autopilot-exit'));
  self.autoPilot.active = false;
};
