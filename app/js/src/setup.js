var WebWallWhispers = WebWallWhispers === undefined ? {} : WebWallWhispers;

(function (W) {
  'use strict';

  W.setUpApplication = function (config) {
    // Debug logs or not
    W.dLog = config.debugLogs;
    // Audio/image media URL
    W.sharedMediaUrl = config.sharedMediaUrl;
    // Image
    W.setUpViewer(config.image);
    // User controls
    W.setDOMListeners(document);
    // Get stream attractors
    if (!W.status.system.mobile) {
      W.streamAttractors = config.attractors;
    } else {
      W.streamAttractors = config.attractorsMobile;
    }
    // Randomize layer 4 streams position
    W.randomizeAttractors(4);
    // Play area playAreaRatios
    W.playAreaRatios = config.playarearatios;
    // Audio config
    W.status.audio.streamsConfig = config.audio;
    // Set up audio
    W.loadAudioData(config.audio);
  };

  W.randomizeAttractors = function (layer) {
    W.streamAttractors[layer].forEach(function(el) {
      if (el.x === "random") {
        el.x = W.randomFloat(0.1, 0.26);
      }
      if (el.y === "random") {
        el.y = W.randomFloat(0.359, 0.39);
      }
    });
  };

  // ******** AUDIO ********
  /**
   * @function createAudioContext create WebAudioAPI context
   **/
  W.createAudioContext = function () {
    var contextClass = (window.AudioContext || window.webkitAudioContext ||
      window.mozAudioContext || window.oAudioContext || window.msAudioContext);
    if (contextClass) {
      var context = new contextClass();
      // Polyfill for stereo panner
      // if (!context.createStereoPanner) {
      //   StereoPannerNode.polyfill();
      // }
      W.status.audio.context = context;
      return context;
    } else {
      // Cannot run app. WebAudioAPI are not supported
      document.dispatchEvent(new CustomEvent('browser-block'));
    }
  };

  /**
   * @function loadAudioData audio data setup
   * @param {Object} audioConfig audio configuration object
   **/
  W.loadAudioData = function (audioConfig) {
    // Create audio context
    var context = W.createAudioContext();
    // Reverberator
    W.reverberator = new Reverberator(context);
    // Load action sounds
    W.loadActionSounds(context);
    // Set streams
    W.wallSounds = W.loadWallStreamSounds(context, audioConfig);
  };

  /**
   * @function loadActionSounds
   * @param {Object} context WebAudioAPI context
   **/
  W.loadActionSounds = function (context) {
    // Load buffers for action sounds
    var actionSoundsURL = W.sharedMediaUrl.concat('/audio/action_sounds/');
    var actionSounds = [
      'tap1.m4a',
      'tap2.m4a',
      'tap3.m4a',
      'tap4.m4a',
      'tap5.m4a',
      'tap6.m4a',
      'tap7.m4a',
      'tap8.m4a',
      'tap9.m4a',
      'tap10.m4a',
      'tap11.m4a',
      'tap12.m4a',
      'tap13.m4a',
      'tap14.m4a',
      'tap15.m4a',
      'tap16.m4a',
      'tap17.m4a',
      'tap18.m4a',
      'tap19.m4a',
      'tap20.m4a',
      'tap21.m4a',
      'tap22.m4a',
      'tap23.m4a',
      'tap24.m4a',
      'tap25.m4a',
      'tap26.m4a',
      'zoomin0.m4a',
      'zoomin1.m4a',
      'zoomin2.m4a',
      'zoomin3.m4a',
      'zoomin4.m4a',
      'zoomout0.m4a',
      'zoomout1.m4a',
      'zoomout2.m4a',
      'zoomout3.m4a',
      'zoomout4.m4a'
    ];
    for (var i = 0; i < actionSounds.length; i++) {
      actionSounds[i] = actionSoundsURL.concat(actionSounds[i]);
    }
    var buffLoader = new BufferLoader(context, actionSounds, W.feedActionAudioNodes);
    buffLoader.load();
  };

  /**
   * @function feedActionAudioNodes init WebAudioAPI nodes from action sounds
   * @param {Array} sounds array of array-buffer containg decoded audio data
   * @param {Object} context WebAudioAPI context
   **/
  W.feedActionAudioNodes = function (sounds, context) {
    for (var i = 0; i < sounds.length; i++) {
      W.actionSounds[i] = new ActionSound(context, sounds[i], i, W.audioLog);
      W.actionSounds[i].init();
    }
    W.zoomSound = W.actionSounds[26]; // first zoom sound
    W.ASfinishLoad = true;
    console.log('Action sounds loading finished');
    if (W.WSfinishLoad && W.OSDfinishLoad) {
      console.log('%c** APP READY **', 'color:darkgreen');
      document.dispatchEvent(new CustomEvent('app-ready'));
    }
  };

  /**
   * @function loadWallStreamSounds stream load for wall sounds
   * @param {Object} context WebAudioAPI context
   * @param {Object} audioConfig audio configuration object
   **/
  W.loadWallStreamSounds = function (context, audioConfig) {
    // Due to bug https://bugs.webkit.org/show_bug.cgi?id=180696 cannot use HLS
    // with Safari Desktop
    if (Hls.isSupported()) {
      var streams = [];
      for (let i = 0; i < audioConfig.streams.length; i++) {
        if (!audioConfig.streams[i].secondary &&
          (W.status.system.mobile || !audioConfig.streams[i].mobile)) {
          streams[i] = new StreamSound(context, i, W.audioLog, W.dLog,W.status.system.mobile);
          streams[i].init(audioConfig.streams[i].hlsPreload,
            audioConfig.streams[i].folderN, W.sharedMediaUrl, audioConfig.streams[i].markers);
          if (audioConfig.streams[i].standBySound) {
            W.standBySound = streams[i];
          }
          if (audioConfig.streams[i].silenceSound) {
            W.silenceSound = streams[i];
          }
          if (audioConfig.streams[i].aleatorySound) {
            W.aleatorySound = streams[i];
          }
          // See all HLS Events - https://goo.gl/e2F3Ki
          // streams[i].hls.on(Hls.Events.FRAG_LOADING, function (event, object) {
          //   // W.log.warn(`Started loading frag ${object.frag.sn} for audio ${i}`);
          // });
          // streams[i].hls.on(Hls.Events.FRAG_LOADED, function (event, object) {
          //   // W.log.warn(`Ended loading frag ${object.frag.sn} for audio ${i}`);
          // });
          // /** BUG: when track changes, if random starting fragment
          //  * is the same of the last played fragment, event is not fired
          //  **/
          // streams[i].hls.on(Hls.Events.FRAG_CHANGED, function (event, object) {
          //   // W.log.info("Fragment "+object.frag.sn+" changed in audio "+i);
          //   var loadTime = performance.now();
          //   if (streams[i].seekTime !== null) {
          //     // W.log.info(`New audio took ${loadTime - streams[i].seekTime} msecs`);
          //     streams[i].seekTime = null;
          //   }
          // });
        } else if (audioConfig.streams[i].secondary) {
          streams[i] = new StreamSound(context, i, W.audioLog, W.dLog,W.status.system.mobile);
          streams[i].initSecondary(streams[audioConfig.streams[i].fromStream], audioConfig.streams[i].markers);
        }
      }
      console.log('Streams initalized');
      W.WSfinishLoad = true;
      return streams;
    } else {
      console.log("Hls not supported");
      if (window.confirm('We suggest the use of Google Chrome for the best experience. Do you want to continue anyways?')) {
        // TODO redirect to lite version
        // remove all handlers, close audio context and call setupLite
      } else {
        console.log('Exiting...');
      }
    }
  };

  /**
   * @function loadBufferedWallSounds AJAX load for wall sounds
   * @param {Object} context WebAudioAPI context
   **/
  W.loadBufferedWallSounds = function (context) {
    // Load buffers for buffered wall sounds
    var baseURL;
    var fileExt = '.m4a';
    var bufferedWallSounds = [
      'layer0',
      'layer1',
      'layer2',
      'layer3',
      'layer4',
      'layer5'
    ];
    if (W.status.system.iOS) {
      console.log('Loading buffered sounds for iOS systems...');
      baseURL = W.sharedMediaUrl.concat('/audio/heaac64/');
    } else {
      console.log('Loading buffered sounds for desktop systems...');
      baseURL = W.sharedMediaUrl.concat('/audio/aac/');
    }
    for (var i = 0; i < bufferedWallSounds.length; i++) {
      bufferedWallSounds[i] = baseURL.concat(bufferedWallSounds[i], fileExt);
    }
    // console.log(bufferedWallSounds);
    var buffLoader = new BufferLoader(context, bufferedWallSounds, W.feedBufferedWallSounds);
    buffLoader.load();
  };

  /**
   * @function feedBufferedWallSounds init WebAudioAPI nodes from buffered sounds
   * @param {Array} sounds array of array-buffer containg decoded audio data
   * @param {Object} context WebAudioAPI context
   **/
  W.feedBufferedWallSounds = function (sounds, context) {
    console.log('feeding buffer sounds nodes');
    for (var i = 0; i < sounds.length; i++) {
      W.wallSounds[i] = new BufferedSound(context, i, W.audioLog, W.dLog);
      W.wallSounds[i].init(sounds[i], W.status.system.mobile);
    }
    W.standBySound = W.wallSounds[0];
    W.WSfinishLoad = true;
    console.log('Buffered wall sounds initalized');
    if (W.ASfinishLoad && W.OSDfinishLoad) {
      console.log('%c** APP READY **', 'color:darkgreen');
      document.dispatchEvent(new CustomEvent('app-ready'));
    }
  };

  // ********* IMAGE VIEWER ********
  /**
   * @function setUpViewer sets image viewer handlers
   * @param {Object} imageConfig image configuration object
   **/
  W.setUpViewer = function (imageConfig) {
    W.img = new SplsViewer(document, imageConfig, W.status.system.mobile,
      W.status.system.iOS);
    W.img.init(imageConfig.maxZoomLevel, W.fullyLoaded);
    W.status.viewer.zoomStartLevel = W.img.getZoomLevel();
    if (W.status.system.mobile) {
      W.setHandlersMobile();
    } else {
      W.setHandlers();
    }
  };

  /**
   * @function fullyLoaded callback for fully loaded image
   **/
  W.fullyLoaded = function () {
    console.log('Fully loaded image');
    W.OSDfinishLoad = true;
    clearTimeout(W.img.resetTimeout);
    if (W.ASfinishLoad && W.WSfinishLoad) {
      console.log('%c** APP READY **', 'color:darkgreen');
      document.dispatchEvent(new CustomEvent('app-ready'));
    }
  };

  /**
   * @function setHandlers sets image viewer handlers
   **/
  W.setHandlers = function () {
    // User actions
    W.img.on('canvas-click', W.manageTap);
    W.img.on('canvas-double-click', W.manageDoubleTap);
    W.img.on('canvas-key', W.manageKeyboard);
    W.img.on('canvas-scroll', W.manageScroll);
    W.img.on('navigator-click', W.manageNavigatorClick);
    // OpenSeadragon
    W.img.on('animation-finish', W.manageAnimationFinish); // Raised when any spring animation ends (zoom, pan, etc.).
    W.img.on('animation-start', W.manageAnimationStart); // Raised when any spring animation starts (zoom, pan, etc.).
    W.img.on('pan', W.managePan); // Raised (continuously) when the viewport is panned
    W.img.on('zoom', W.manageZoom); // Raised (continuously) when the viewport zoom level changes
    W.img.on('update-viewport', W.updateViewer);
    // W.img.on('animation', function(){// console.log('animation');}); // Raised when any spring animation update occurs (zoom, pan, etc.), after the viewer has drawn the new location
    // W.img.on('viewport-change', function(){// console.log('viewport change');}); // Raised when any spring animation update occurs (zoom, pan, etc.), before the viewer has drawn the new location
    // W.img.on('add-overlay', ...); // Raised when an overlay is added to the viewer
    // W.img.on('remove-overlay', ...); // Raised when an overlay is removed from the viewer
  };

  /**
   * @function setHandlersMobile sets image viewer handlers for mobile version
   **/
  W.setHandlersMobile = function () {
    // User actions
    W.img.on('canvas-click', W.manageMobileTap);
    W.img.on('canvas-pinch', W.manageMobilePinch);
    //W.img.on('canvas-double-click', W.manageDoubleTap);
    W.img.on('canvas-drag', W.manageMobileDrag);
    W.img.on('canvas-drag-end', W.manageMobileDragEnd);
    // OpenSeadragon
    W.img.on('animation-finish', W.manageAnimationFinish); // Raised when any spring animation ends (zoom, pan, etc.).
    W.img.on('animation-start', W.manageAnimationStart); // Raised when any spring animation starts (zoom, pan, etc.).
    W.img.on('pan', W.manageMobilePan); // Raised (continuously) when the viewport is panned
    W.img.on('zoom', W.manageMobileZoom); // Raised (continuously) when the viewport zoom level changes
    W.img.on('update-viewport', W.updateViewer);
  };


  // ******** DOCUMENT ********
  /**
   * @function setDOMListeners sets listeners to DOM elements
   **/
  W.setDOMListeners = function () {
    document.getElementById('muteButton').addEventListener('click', function () {
      if (!W.status.audio.appMuted) {
        W.muteAll();
        this.setAttribute("class", "unMuteButton");
      } else if (W.status.audio.appMuted) {
        W.unMuteAll();
        this.setAttribute("class", "muteButton");
      }
    });
    document.getElementById('resetViewButton').addEventListener('click', function () {
      W.img.goHome();
    });
    W.splash.addEventListener('click', W.enter);
    // document.getElementById('resetAudioLog').addEventListener('click', function () {
    //   document.getElementById('audioLogs').innerHTML = '';
    // });
    document.addEventListener('keydown', function () {
      var haveKeyboardFocus = document.activeElement == W.img.viewer.canvas;
      if (!haveKeyboardFocus) {
        W.img.viewer.canvas.focus();
      }
    });
  };

  W.setUpLiteApplication = function (config) {
    console.log('setting up lite app');
    // Debug logs or not
    W.dLog = config.debugLogs;
    // Audio/image media URL
    W.sharedMediaUrl = config.sharedMediaUrl;
    // Image
    W.img = new SplsViewer(document, config.image, W.status.system.mobile,
      W.status.system.iOS);
    W.img.init(config.image.maxZoomLevel, W.fullyLoaded);
    W.status.viewer.zoomStartLevel = W.img.getZoomLevel();
    if (W.status.system.mobile) {
      W.setHandlersiLiteMobile();
    } else {
      W.setHandlersLiteDesktop();
    }
    // User controls
    W.setDOMListeners(document);
    // Set up audio
    var context = W.createAudioContext();
    W.loadActionSounds(context);
    W.loadBufferedWallSounds(context);
  };

  /**
   * @function setHandlersLiteDesktop sets image viewer handlers
   **/
  W.setHandlersLiteDesktop = function () {
    // User actions
    W.img.on('canvas-click', W.manageTap);
    W.img.on('canvas-double-click', W.manageDoubleTap);
    W.img.on('canvas-key', W.manageKeyboard);
    W.img.on('canvas-scroll', W.manageScroll);
    W.img.on('navigator-click', W.manageNavigatorClick);
    // OpenSeadragon
    W.img.on('animation-finish', W.manageAnimationFinishLite); // Raised when any spring animation ends (zoom, pan, etc.).
    W.img.on('animation-start', W.manageAnimationStart); // Raised when any spring animation starts (zoom, pan, etc.).
    W.img.on('zoom', W.manageZoom); // Raised (continuously) when the viewport zoom level changes
  };

  /**
   * @function setHandlersiLiteMobile sets image viewer handlers
   **/
  W.setHandlersiLiteMobile = function () {
    // User actions
    W.img.on('canvas-click', W.manageMobileTap);
    W.img.on('canvas-pinch', W.manageMobilePinch);
    W.img.on('canvas-drag', W.manageMobileDrag);
    W.img.on('canvas-drag-end', W.manageMobileDragEnd);
    // OpenSeadragon
    W.img.on('animation-finish', W.manageAnimationFinishLite); // Raised when any spring animation ends (zoom, pan, etc.).
    W.img.on('animation-start', W.manageAnimationStart); // Raised when any spring animation starts (zoom, pan, etc.).
    W.img.on('zoom', W.manageMobileZoom); // Raised (continuously) when the viewport zoom level changes
  };

})(WebWallWhispers);
