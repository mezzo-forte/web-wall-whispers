// Web Wall Whispers
//
// Interactive web-based audio-visual installation.
// Developed for Fondazione Spinola-Banna per l'Arte.
// Copyright (C) 2018 Francesco Cretti
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see
// https://bitbucket.org/splsteam/splswebapp/src/master/LICENSE.txt.
//
// JS developer - francesco.cretti@gmail.com
// Coordinator - leehooni@gmail.com
//
// Home Page: www.webwallwhispers.net
// Bug report: https://bitbucket.org/splsteam/splswebapp/issues

// *********************************************************
/**
 * @function BufferedSound Buffered SOUND CONSTRUCTOR
 * @param {Object} audioContext WebAudio Context of current application
 * @param {Number} idx Sound index
 * @param {Object} HTMLlog DOM element for logging audio status
 * @param {Boolean} debugLog True to activate console debug logs
 **/
function BufferedSound(audioContext, idx, HTMLlog, debugLog) {
	this.context = audioContext;
	this.index = idx;
	this.htmlLogger = HTMLlog;
	this.dLog = debugLog;
	this.secondaryStream = undefined;
	this.secondary = false;
	this.sourceNode = undefined;
	this.masterGain = undefined;
  this.playing = false;
	this.fadingIn = false;
	this.fadingOut = false;
	this.gainRandom = false;
	this.randomizeGain = false;
	this.loop = false;
	this.reverb = false;
	this.reverberator = undefined;
	// this.filter = new Filter(audioContext, this);
	// this.distortion = new Distortion(audioContext, this);
	// this.binauralizator = new Binauralizator(audioContext, this);
	// this.stereoPanner = new StereoPanner(audioContext, this);
}

/**
 * @function init
 * @param {ArrayBuffer} buffer Actual array buffer with decoded audio data
 * @param {Boolean} mobile true if mobile device
 **/
BufferedSound.prototype.init = function (buffer, mobile) {
	this.buffer = buffer;
	this.mobile = mobile;
	this.muteNode = this.context.createGain();
	this.masterGain = this.context.createGain();
	this.muteNode.connect(this.masterGain);
	this.masterGain.connect(this.context.destination);
	this.masterGain.gain.setValueAtTime(0, this.context.currentTime);
	this.setLoop(true);
	//console.log('Buffered SOUND INIT');
};

/**
 * @function initSecondary
 * @param {BufferedSound} origStream original stream
 **/
BufferedSound.prototype.initSecondary = function (origStream, mobile) {
	const self = this;
	self.secondary = true;
	self.mobile = mobile;
	// Hold reference to secondary stream
	self.secondaryStream = origStream;
	origStream.secondaryStream = self;
	self.init(origStream.buffer);
	self.dLog && console.log(`Secondary buffered sound ${self.index} derived from stream ${origStream.index}`);
};

/**
 * @function setBufferAndStart
 * @param {Boolean} sprite true if sprite, false if whole file
 **/
BufferedSound.prototype.setBufferAndStart = function (sprite) {
	const self = this;
	self.sourceNode = self.context.createBufferSource();
	self.sourceNode.buffer = self.buffer;
	if (sprite === undefined) {
		self.sourceNode.loop = self.loop;
		self.sourceNode.start(0);
	} else if (sprite.loop) {
		self.sourceNode.loopStart = sprite.start;
		self.sourceNode.loopEnd = sprite.end;
		self.sourceNode.loop = true;
		self.sourceNode.start(sprite.start);
	} else {
		self.sourceNode.start(0, sprite.start);
		setTimeout(function () {
			self.fadeOut(1);
		}, sprite.duration * 1000);
	}
	self.sourceNode.connect(self.muteNode);
};

/**
 * @function play
 * @param {Boolean} sprite true if sprite, false if whole file
 **/
BufferedSound.prototype.play = function (sprite) {
	if (!this.playing) {
		// If has not playing secondary
		if (this.secondaryStream === undefined) {
			// Create source node and start
			this.dLog && console.log(`No secondary streams -> play`);
			this.setBufferAndStart(sprite);
		} else if (!this.secondaryStream.isPlaying()) {
			// If not playing secondary streams -> create source node and start
			this.dLog && console.log(`Not playing secondary streams -> play`);
			this.setBufferAndStart(sprite);
		} else {
			// If already playing secondary stream -> connect to secondary source
			this.sourceNode = this.secondaryStream.sourceNode;
			this.sourceNode.connect(this.muteNode);
		}
		this.playing = true;
		this.dLog && console.log(`Buffered sound ID: ${this.index} - Play`);
		// this.htmlLogger.innerHTML += '<br>Played Buffered sound ID: ' + this.index;
	} else {
		this.dLog && console.log('Already playing. Check gain value');
	}
};

/**
 * @function stop
 **/
BufferedSound.prototype.stop = function () {
	if (this.playing) {
		// If has not secondary streams
		if (this.secondaryStream === undefined) {
			this.dLog && console.log(`No secondary streams -> stop`);
			this.sourceNode.stop(0);
			this.sourceNode.disconnect();
		} else if (!this.secondaryStream.isPlaying()) {
			this.dLog && console.log(`Not playing secondary streams -> stop`);
			this.sourceNode.stop(0);
			this.sourceNode.disconnect();
		} else {
			this.dLog && console.log(`Playing secondary streams -> don't disconnect`);
			// TODO test application without this comment
			// this.sourceNode.disconnect(this.muteNode);
		}
		this.playing = false;
		this.dLog && console.log(`Buffered sound ID: ${this.index} - stopped`);
		// this.htmlLogger.innerHTML += '<br>Stopped Buffered sound <ID: ' + this.index + '> ';
	} else {
		this.dLog && console.log(`Buffered sound ID: ${this.index} - stopped`);
	}
};

/**
 * @function setLoop
 * @param {Boolean} loop false for loop
 **/
BufferedSound.prototype.setLoop = function (loop) {
	const self = this;
	// Sets loop when starts playing
	self.loop = loop;
	self.dLog && console.log(`Buffered sound ID: ${this.index} - loop set to ${loop}`);
};

/**
 * @function fadeIn
 * @param {Number} fadeTime Duration of fade in seconds
 * @param {Boolean} random start from random point
 * @param {Boolean} sprite true if sprite, false if whole file
 **/
BufferedSound.prototype.fadeIn = function (fadeTime, random, sprite) {
	const self = this;
	if (!self.playing) {
		if (!fadeTime) {
			self.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			fadeTime = 1;
		}
		if (self.randomizeGain && !self.gainRandom) {
			self.gainRandomizerStart();
		}
		var curTime = self.context.currentTime;
		self.masterGain.gain.cancelScheduledValues(curTime);
		self.masterGain.gain.setValueAtTime(0.0001, curTime);
		self.masterGain.gain.linearRampToValueAtTime(1.0, curTime + fadeTime);
		self.fadingIn = true;
		self.play(sprite);
		self.htmlLogger.innerHTML += '<br>Buffered sound fading in <ID: ' + self.index + '>... ';
		setTimeout(function () {
			self.fadingIn = false;
			self.dLog && console.log(`Fade in stream finished [ ID: ${self.index} ]`);
			self.htmlLogger.innerHTML += '<br>Fade in stream finished [ID: ' + self.index + '] ';
		}, fadeTime * 1000 + 25);
	} else {
		self.dLog && console.log(`Cannot fade in. Already playing [ ID: ${self.index} ]`);
	}
};

/**
 * @function fadeOut
 * @param {Number} fadeTime Duration of fade in seconds
 **/
 BufferedSound.prototype.fadeOut = function (fadeTime) {
 	const self = this;
 	if (self.playing) {
		if (!fadeTime) {
			self.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			fadeTime = 1;
		}
		if (self.gainRandom) {
			self.gainRandomizerStop();
		}
 		var currGain = self.masterGain.gain.value;
 		var curTime = self.context.currentTime;
		self.masterGain.gain.cancelScheduledValues(curTime);
		self.masterGain.gain.setValueAtTime(currGain, curTime);
		self.masterGain.gain.linearRampToValueAtTime(0.0001, curTime + fadeTime);
 		self.fadingOut = true;
 		setTimeout(function () {
			self.stop();
 			self.fadingOut = false;
 			self.dLog && console.log(`Fade out buffered sound finished [ ID: ${self.index} ]`);
 			// self.htmlLogger.innerHTML += '<br>Fade out buffered sound finished [ID: ' + self.index + '] ';
 		}, fadeTime * 1001);
		self.dLog && console.log(`Fading out buffered sound [ ID: ${self.index} ]`);
		// self.htmlLogger.innerHTML += '<br>Buffered sound fading out <ID: ' + self.index + '>... ';
 	} else {
		self.dLog && console.warn(`Cannot fade out. Sound not playing [ ID: ${self.index} ]`);
	}
 };

/**
 * @function isFadingIn
 * @returns {Boolean} If stream is currently fading in
 **/
BufferedSound.prototype.isFadingIn = function () {
	return this.fadingIn;
};

/**
 * @function isFadingOut
 * @returns {Boolean} If stream is currently fading out
 **/
BufferedSound.prototype.isFadingOut = function () {
	return this.fadingOut;
};

/**
 * @function isFading
 * @returns {Boolean} If stream is currently fading in or out
 **/
BufferedSound.prototype.isFading = function () {
	return (this.fadingOut || this.fadingOut);
};

/**
 * @function isPlaying
 * @returns {Boolean} If stream is currently fading
 **/
BufferedSound.prototype.isPlaying = function () {
	return this.playing;
};

/**
 * @function mute
 **/
BufferedSound.prototype.mute = function () {
	this.muteNode.gain.setValueAtTime(0, this.context.currentTime);
};

/**
 * @function unmute
 **/
BufferedSound.prototype.unmute = function () {
		this.muteNode.gain.setValueAtTime(1, this.context.currentTime);
};

/**
 * @function setMasterGain
 * @param {Number} mgain Gain value of master signal
 **/
BufferedSound.prototype.setMasterGain = function (mgain) {
	if (mgain > 1) {
		this.dLog && console.warn('Gain values cannot be greater than 1');
		mgain = 1;
	} else if (mgain < 0) {
		this.dLog && console.warn('Gain values cannot be lower than 0');
		mgain = 0;
	}
	this.masterGain.gain.setTargetAtTime(mgain, this.context.currentTime, 0.5);
};

/**
 * @function getMasterGain
 * @returns {Number} master gain value
 **/
BufferedSound.prototype.getMasterGain = function () {
	return this.masterGain.gain.value;
};

/**
 * @function fadeToGain
 * @param {Number} val Final gain value
 * @param {Number} secs Duration of fade in seconds
 **/
BufferedSound.prototype.fadeToGain = function (val, secs) {
		if (val > 1) {
			this.dLog && console.warn('Gain values cannot be greater than 1');
			val = 1;
		} else if (val < 0) {
			this.dLog && console.warn('Gain values cannot be lower than 0');
			val = 0;
		}
		if (!secs) {
			this.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
			secs = 1;
		}
		this.masterGain.gain.setValueAtTime(this.masterGain.gain.value, this.context.currentTime);
		this.masterGain.gain.linearRampToValueAtTime(val, this.context.currentTime + secs);
};

/**
 * @function addReverb
 * @param {Object} rvb Reverberator object to connect to
 * @param {Number} wgain Gain value of wet signal
 * @returns {Boolean} True if reverb added / Flase if already present
 **/
BufferedSound.prototype.addReverb = function (rvb, wgain) {
	const self = this;
	if (!self.reverb) {
		if (wgain > 1) {
			self.dLog && console.warn('Gain values cannot be greater than 1');
			wgain = 1;
		} else if (wgain < 0) {
			self.dLog && console.warn('Gain values cannot be lower than 0');
			wgain = 0;
		}
		self.reverberator = rvb;
		// create gain node for adjusting wet signal
		self.wetGain = self.context.createGain();
		self.wetGain.gain.setTargetAtTime(wgain, self.context.currentTime, 0.5);
		// Do connections
		self.masterGain.connect(self.wetGain);
		self.wetGain.connect(self.reverberator.convolver);
		self.reverb = true;
		self.dLog && console.log(`Reverb added [ID: ${self.index}]`);
		return true;
	} else {
		self.dLog && console.warn(`Cannot add reverb. Sound already reverberated [ID: ${self.index}]`);
		return false;
	}
};

/**
 * @function removeReverb
 * @returns {Boolean} True if reverb removed / False if not
 **/
BufferedSound.prototype.removeReverb = function () {
		if (this.reverb) {
			this.masterGain.disconnect(this.wetGain);
			this.wetGain.disconnect(this.reverberator.convolver);
			this.dLog && console.log(`Reverb removed [ID: ${this.index}]`);
			this.reverb = false;
			return true;
		} else {
			this.dLog && console.warn(`Cannot remove reverb. Sound not reverberated [ID: ${this.index}]`);
			return false;
		}
};

/**
 * @function setWetGain
 * @param {Number} wgain Gain value for wet signal
 **/
BufferedSound.prototype.setWetGain = function (wgain) {
	if (this.reverb) {
		this.wetGain.gain.setTargetAtTime(wgain, this.context.currentTime, 0.5);
	} else {
		this.dLog && console.warn(`Cannot set wet gain. Sound not reverberated [ID: ${this.index}]`);
	}
};

/**
 * @function fadeOutReverb
 * @param {Number} fadeTime Fade time in seconds
 **/
BufferedSound.prototype.fadeOutReverb = function (fadeTime) {
	const self = this;
	if (!fadeTime) {
		this.dLog && console.warn(`Missing argument fadeTime. Set to 1 second`);
		fadeTime = 1;
	}
	if (self.reverb) {
		self.wetGain.gain.setValueAtTime(self.wetGain.gain.value, self.context.currentTime);
		self.wetGain.gain.linearRampToValueAtTime(0.0001, self.context.currentTime + fadeTime);
		setTimeout(function () {
			self.removeReverb();
		}, fadeTime * 1000);
	} else {
		self.dLog && console.warn(`Cannot fade out reverb. Sound not reverberated [ID: ${self.index}]`);
	}
};

/**
 * @function playSprite
 * @param {Number} startTime
 * @param {Number} stopTime
 * @param {Boolean} loop
 **/
BufferedSound.prototype.playSprite = function (startTime, stopTime, loop) {
	const self = this;
	var dur = stopTime - startTime;
	var sprite = {
		loop: loop,
		start: startTime,
		end: stopTime,
	  duration: dur
	};
	if (!self.isPlaying()) {
		self.fadeIn(1, false, sprite);
	}
};

/**
 * @function gainRandomizerStart produces random sequence to module master gain
 **/
BufferedSound.prototype.gainRandomizerStart = function () {
	const self = this;
	if (!self.gainRandom) {
		self.gainRandom = true;
		var i = 0;
		(function set(){
			//var gain = (Math.sin(self.context.currentTime) + 1) * 0.375 + 0.25;
			var gain = Math.random();
			self.dLog &&  console.log(`random gain = ${gain} ID: ${self.index}`);
			// self.fadeToGain(gain, 1);
			self.setMasterGain(gain);
			self.gainRandomId = setTimeout(set, 5000);
			//console.log(`oscill ID ${self.gainOscillId}`);
		}());
	}
};

/**
 * @function gainRandomizerStop
 **/
BufferedSound.prototype.gainRandomizerStop = function () {
	if (this.gainRandom) {
		clearTimeout(this.gainRandomId);
		this.setMasterGain(1);
		this.gainRandom = false;
	}
};

/**
 * @function setEffectChain
 * @param {Object} options
 * @param {Object} rvb Reverberator object to connect to
 **/
BufferedSound.prototype.setEffectChain = function (options, rvb) {
	// Default values
	let defaults = {
		randomizeGain: false,
		lpFilter: true,
		lpFreq: 10000,
		hpFilter: false,
		hpFreq: 500,
		reverb: true,
		wetGain: 0.5,
		distortion: false,
		distAmount: 50,
		binaural: false,
		stereoPanner : false
	};
	let actual = Object.assign({}, defaults, options);
	/**
	 * Effect chain is always connected as follows:
	 * Source (Master Gain) -> LP filter -> HP filter -> Dist -> Binaural
	 * irrespective of their instertion order
	 **/
	 const self = this;
	 // Randomize gain
	 if (actual.randomizeGain) {
		 // self.gainRandomizerStart();
		 self.randomizeGain = true;
	 } else {
		 // self.gainRandomizerStop();
		 self.randomizeGain = false;
	 }
	 // Binaural
	 if (actual.binaural) {
		 self.binauralizator.makeBinaural();
	 } else {
		 self.binauralizator.removeBinaural();
	 }
	 // StereoPanner
	 if (self.mobile) {
		 if (actual.stereoPanner) {
			 self.stereoPanner.addSteroPanner();
		 } else {
			 self.stereoPanner.removeStereoPanner();
		 }
	 }
	 // Low pass filter
	 setTimeout(self.checkLPfilter.bind(self, actual), 300);
	 // High pass filter
	 setTimeout(self.checkHPfilter.bind(self, actual), 600);
	 // Distortion
	 setTimeout(self.checkDistortion.bind(self, actual), 900);
	 // Reverb is independent
	 setTimeout(self.checkReverb.bind(self, actual, rvb), 1200);
	 self.dLog && console.log(`Effect chain schduled [ID: ${self.index}]`);
 };

/**
 * @function checkLPfilter
 * @param {Object} options
 **/
BufferedSound.prototype.checkLPfilter = function (options) {
	 this.dLog && console.log('Checking LP filter...');
	 if (options.lpFilter) {
		 this.dLog && console.log(`Setting LP filter to ${options.lpFreq}...`);
		 if (!this.filter.lowPassFilter(options.lpFreq)) {
			 // If filter already present fade to target frequency
			 this.filter.fadeToLPFrequency(options.lpFreq, 3);
		 }
	 } else {
		 this.filter.removeLowPassFilter();
	 }
 };

/**
 * @function checkHPfilter
 * @param {Object} options
 **/
BufferedSound.prototype.checkHPfilter = function (options) {
	this.dLog && console.log('Checking HP filter...');
	if (options.hpFilter) {
		this.dLog && console.log(`Setting HP filter to ${options.hpFreq}...`);
		if (!this.filter.highPassFilter(options.hpFreq)) {
			// If filter already present fade to target frequency
			this.filter.fadeToHPFrequency(options.hpFreq, 3);
		}
	} else {
		this.dLog && console.log('Removing HP filter...');
		this.filter.removeHighPassFilter();
	}
};

/**
 * @function checkDistortion
 * @param {Object} options
 **/
BufferedSound.prototype.checkDistortion = function (options) {
	this.dLog && console.log('Checking Distortion...');
	if (options.distortion) {
		this.dLog && console.log(`Setting Distortion to ${options.distAmount}...`);
		if (!this.distortion.addDistortion(options.distAmount)) {
			// If distortion already present, change amount value
			this.distortion.setDistAmount(options.distAmount);
		}
	} else {
		this.distortion.removeDistortion();
	}
};

/**
 * @function checkReverb
 * @param {Object} options
 * @param {Object} rvb
 **/
BufferedSound.prototype.checkReverb = function (options, rvb) {
	this.dLog && console.log('Checking Reverb...');
	if (options.reverb) {
		if (!this.addReverb(rvb, options.wetGain)) {
			// If reverb already present change wet gain
			this.setWetGain(options.wetGain);
		}
	} else {
		this.fadeOutReverb(3);
	}
};
